//
//  NotificationViewController.swift
//  Loop
//
//  Created by Jorch Wong on 17/4/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import UserNotifications



class NotificationViewController: UIViewController {

    @IBOutlet var mondaySwitch: UISwitch!
    @IBOutlet var tuesdaySwitch: UISwitch!
    @IBOutlet var wednesdaySwitch: UISwitch!
    @IBOutlet var thursdaySwitch: UISwitch!
    @IBOutlet var fridaySwitch: UISwitch!
    @IBOutlet var saturdaySwitch: UISwitch!
    @IBOutlet var sundaySwitch: UISwitch!
    var notificationAddedCompletionBlock:((String)->Void)?
    
//    var selectedDaysArray = NSMutableArray()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationManager.shared.getPendingNotifications { (identifier) in
            self.setupview(dayStr: identifier)
        }
    }
    
    
    func setupview(dayStr:String) {
        
        DispatchQueue.main.sync {
            switch dayStr {
            case "monday":
                self.mondaySwitch.isOn = true
                break
            case "tuesday":
                self.tuesdaySwitch.isOn = true
                break
            case "wednesday":
                self.wednesdaySwitch.isOn = true
                break
            case "thursday":
                self.thursdaySwitch.isOn = true
                break
            case "friday":
                self.fridaySwitch.isOn = true
                break
            case "saturday":
                self.saturdaySwitch.isOn = true
                break
            case "sunday":
                self.sundaySwitch.isOn = true
                break
            default:
                break
            }
        }

        
//        monday,tuesday,wednesday,thursday,friday,saturday,sunday
       
    }
    
    
    
    func notificationErrorAlert() {

        let alert = UIAlertController(title: "錯誤", message: "無法安排通知。 請再試一次。", preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "好", style: .default) { _ in
       
        }
        
        alert.addAction(deleteAction)
        present(alert, animated: true, completion: nil)
    }
    
    
    func updateNotifications(sender:UISwitch,identifier:String) {
        
        
        NotificationManager.shared.checkAndCreateNewNotification(sender: sender,identifier: identifier)

        
        guard let userId = UserDataManager.userId() else { return }
        
        let statusStr = sender.isOn ? "true":"false"
        
        let params = ["user_id":userId,"status":statusStr,"notification_key":identifier] as [String : Any]
        
        print(params)

        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: UPDATE_NOTIFICATIONS) { (response, success) in
            
            IndicatorManager.stopIndicator()
            
            print(response)
            
            if success {
                let userData = response.value(forKey: "data")
                UserDataManager.saveData(userDic: userData as! [String:AnyObject])
            }else{
                sender.setOn(false, animated: true)
                
            }
        }
    }
    
    
    @IBAction func sunday(_ sender: UISwitch) {
        print("sunday")
        self.updateNotifications(sender: sender, identifier: "sunday")
    }
    
    @IBAction func mon(_ sender: UISwitch) {
        self.updateNotifications(sender: sender, identifier: "monday")
    }
 
    @IBAction func tue(_ sender: UISwitch) {
        self.updateNotifications(sender: sender, identifier: "tuesday")
    }
    
    @IBAction func wednesday(_ sender: UISwitch) {
        self.updateNotifications(sender: sender, identifier: "wednesday")
    }
    
    @IBAction func thursday(_ sender: UISwitch) {
        self.updateNotifications(sender: sender, identifier: "thursday")
    }
    
    
    @IBAction func friday(_ sender: UISwitch) {
        self.updateNotifications(sender: sender, identifier: "friday")
    }
    
    @IBAction func saturday(_ sender: UISwitch) {
        self.updateNotifications(sender: sender, identifier: "saturday")
    }
    

}
