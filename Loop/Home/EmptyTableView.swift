//
//  EventsHeaderView.swift
//  Gifts&Bakes
//
//  Created by KHOSA on 23/07/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class EmptyTableView: UIView {

    
    
    @IBOutlet var imageView:UIImageView!
    @IBOutlet var phoneIconImageView:UIImageView!
    @IBOutlet var button:UIButton!
    
    var deletePressed:(()->Void)?

    
    class func instanceFromNib() -> EmptyTableView {
        return UINib(nibName: "EmptyTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyTableView
    }
    
    func setUpView()  {
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(viewTapped))
//        self.deleteView.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped()  {
        self.deletePressed!()
    }
    

}
