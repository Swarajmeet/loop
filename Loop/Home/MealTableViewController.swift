//
//  MealTableViewController.swift
//  Loop
//
//  Created by Jorch Wong on 31/12/2018.
//  Copyright © 2018 Jorch Wong. All rights reserved.
//

import UIKit
import UserNotifications
import AccountKit
import DZNEmptyDataSet


class MealTableViewController: UITableViewController {
    
    //MARK: Properties
    var meals = [Meal]()
  
    var ordersList = [OrderList]()

    var _accountKit = AccountKit(responseType: .accessToken)

    @IBOutlet var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.setTableBackgroungImage(set: true)
    }
    
    func setTableBackgroungImage(set:Bool)  {
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        let tempImageView = UIImageView(image: UIImage(named: "Background"))
        tempImageView.frame = self.tableView.frame
        self.tableView.backgroundView = tempImageView
    }
    
    func resetTableBackground()  {
        self.tableView.backgroundView = nil
        self.tableView.emptyDataSetSource = nil
        self.tableView.emptyDataSetDelegate = nil
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.getOrdersListAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func chooseBinImage(binId:String) -> UIImage {
        
        switch binId {
        case "1":
            return UIImage.init(named: "blackBinUnSelected")!
        case "2":
            return UIImage.init(named: "whiteBinUnSelected")!
        case "3":
            return UIImage.init(named: "greenBinUnSelected")!
        default:
            return UIImage.init(named: "blackBinUnSelected")!
        }
    }
    
    
    @IBAction func addOrderButtonAction(_ sender: Any) {
        
        if UserDataManager.userData() == nil {
            self.loginWithPhone()
        }else{
            self.presentAddOrderScreen()
        }
    }
    
    func presentAddOrderScreen()  {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "AddOrderNavigation")
        self.present(destination!, animated: true, completion: nil)
    }
    

    
    //MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    //MARK: Actions
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        self.logoutWarning()
    }

    
    @IBAction func unwindToOrdersList(sender: UIStoryboardSegue) {
//       self.getOrdersListAPI()
    }
    
    func countryName(from countryCode: String) -> String {
      
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            return name
        } else {
            return countryCode
        }
    }
    
    // MARK:: ALERT METHODS
    
    func logoutWarning() {

        let alert = UIAlertController(title: "登出", message: "你确定要戒掉吗？", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "否", style: .cancel, handler: nil)
        
        let deleteAction = UIAlertAction(title: "是", style: .destructive) { _ in
            DispatchQueue.main.async {
                self.logoutAPI()
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        present(alert, animated: true, completion: nil)
    }
    
    
    func showDeleteWarning(for indexPath: IndexPath) {

        let alert = UIAlertController(title: "停止回收服務", message: "你確定要終止回收服務嗎？", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "否", style: .cancel, handler: nil)
        
        let deleteAction = UIAlertAction(title: "是", style: .destructive) { _ in
            DispatchQueue.main.async {
                
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        present(alert, animated: true, completion: nil)
    }

    
    // MARK:: API METHODS

    func getOrdersListAPI()  {
        
        self.logoutButton.isHidden = true
        
        guard let userid = UserDataManager.userId() else {
            self.setTableBackgroungImage(set: false)
            return
        }
        
        
        let params = ["user_id":userid]
        
        
        self.logoutButton.isHidden = false
        
        IndicatorManager.startIndicator()
       
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: GET_ORDERS) { (resultDic, success) in
            
            print(resultDic)
            
            self.resetTableBackground()
            self.setTableBackgroungImage(set: false)

            
            if success{
                do {
                    let data = try JSONSerialization.data(withJSONObject: resultDic, options: .prettyPrinted)
                    let streetDataReponse = try JSONDecoder().decode(UserOrdersData.self, from: data)
                    self.ordersList = streetDataReponse.data
                    self.tableView.reloadData()
                    
                } catch let jsonErr {
                    print("Error serializing json: ", jsonErr)
                }
            }else{
//                self.tableView.backgroundView = nil
//                self.setTableBackgroungImage(set: false)
              }
        }
    }
    
    func signInUserAPI(params:[String:AnyObject])  {
        
        APIManager.shared.postDataToServer(params, url: SIGNUP_USER_URL) { (response, success) in

            IndicatorManager.stopIndicator()

            if success{
                
                print(response)
                let userData = response.value(forKey: "data")
                self.resetTableBackground()

                self.setTableBackgroungImage(set: false)

                UserDataManager.saveData(userDic: userData as! [String:AnyObject])
                self.presentAddOrderScreen()
            }
        }
    }
    
    func logoutAPI()  {
        
        guard let userid = UserDataManager.userId() else { return  }
        let params = ["user_id":userid]
        
        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: LOGOUT_URL) { (response, success) in
            
            IndicatorManager.stopIndicator()
            
            if success{
                self.ordersList.removeAll()
                self.resetTableBackground()
                self._accountKit.logOut()
                UserDataManager.clear()
                self.logoutButton.isHidden = true
                self.setTableBackgroungImage(set: false)
            }
        }
    }
    
}


extension MealTableViewController: AKFViewControllerDelegate {
    
    func loginWithPhone(){
        let vc = _accountKit.viewControllerForPhoneLogin()
        vc.isSendToFacebookEnabled = true
        vc.isGetACallEnabled = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as UIViewController, animated: true, completion: nil)
    }
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        //Costumize the theme
        
        
//        143 206 197
        
        let theme:Theme = Theme.default()
        theme.headerBackgroundColor = .loopGreenColor
        theme.headerTextColor = UIColor.white
        theme.headerTextType = .appName
        theme.inputTextColor = UIColor.black.withAlphaComponent(0.87)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor.black.withAlphaComponent(0.87)
        loginViewController.setTheme(theme)
    }

    
    func getAccountDetail()  {
        
        
        IndicatorManager.startIndicator(meesageStr: "Logging in...")
        
        _accountKit.requestAccount{
            (account, error) -> Void in
            
            if error != nil{
                IndicatorManager.stopIndicator()
                
            }else if account?.phoneNumber?.phoneNumber != nil {
                
                let mobile_no = account?.phoneNumber?.phoneNumber
                let country_code = account?.phoneNumber?.countryCode
                let country_name = self.countryName(from: country_code!)
                let account_id = account?.accountID
                
                
                let params:[String:String] = ["phone_number":mobile_no!,
                              "country_code":country_code!,
                              "country_name":country_name,
                              "os_name":os_Name,
                              "os_version":os_Version,
                              "device_manufacture":device_Manufacture,
                              "device_model":device_Model,
                              "device_token":device_token,
                              "model_number":model_No,
                              "account_id":account_id!]
                
                print(params)
                
                self.signInUserAPI(params: params as [String : AnyObject])
                
//                self.signInUserAPI()(parameters: params)
            }
        }
    }
  
    // MARK: AKFViewControllerDelegate
    
    private func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AccessToken!, state: String!) {
        
        print("did complete login with access token \(accessToken.tokenString) state \(String(describing: state))")
    }
    
    // handle callback on successful login to show authorization code
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith code: String, state: String) {
        print("didCompleteLoginWithAuthorizationCode")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)) {
        // ... handle user cancellation of the login process ...
        print("viewControllerDidCancel")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didFailWithError error: Error) {
        // ... implement appropriate error handling ...
        print("\(String(describing: viewController)) did fail with error: \(error.localizedDescription)")
    }
    
   
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AccessToken, state: String) {
        
        print("didCompleteLoginWith")
        print(state as Any)
        
        self.getAccountDetail()
    }
}


extension MealTableViewController {
    
    //MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ordersList.count
    }
    
    //MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "MealTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MealTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        
        let orderModel = self.ordersList[indexPath.row]
        cell.nameLabel.text = orderModel.user_name
        cell.phoneNumberLabel.text = orderModel.phone_number
        cell.streetName.text = orderModel.street_name
        cell.buildingName.text = orderModel.building_name
        cell.floorName.text = orderModel.floor_number
        cell.flatName.text = orderModel.flat_number
        cell.binImageView.image = self.chooseBinImage(binId: orderModel.recycle_bin_id!)
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    // Cancelling the Services
    @available(iOS 11.0, *)
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let modifyAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            DispatchQueue.main.async {
                self.showDeleteWarning(for: indexPath)
            }
            success(true)
        })
        
        modifyAction.image = UIImage(named: "delete")
        modifyAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [modifyAction])
    }

}

extension MealTableViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
       
        return UIImage.init(named: "noProduct")
    }

    
//    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
//        let attributes = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20),NSAttributedString.Key.foregroundColor:UIColor.darkGray]
//        let attString = NSAttributedString.init(string: "没有订单", attributes: attributes)
//        return attString
//    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        
        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20),NSAttributedString.Key.foregroundColor:UIColor.black]
        let attString = NSAttributedString.init(string: self.buttonTitle(), attributes: attributes)
        return attString
    }
    
    func buttonTitle() -> String {
        
        if UserDataManager.userId() != nil {
            return "現在下單"
        }else{
            return "立即登入"
        }
        
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        
        print(button.titleLabel?.text as Any)
        
        self.addOrderButtonAction(button)
    }
}

