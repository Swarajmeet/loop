//
//  MealViewController.swift
//  Loop
//
//  Created by Jorch Wong on 31/12/2018.
//  Copyright © 2018 Jorch Wong. All rights reserved.
//

import UIKit
import os.log
import UserNotifications
import PassKit
import Stripe
import IQKeyboardManagerSwift
import SafariServices


class MealViewController: UIViewController, UINavigationControllerDelegate,SFSafariViewControllerDelegate {
    
    //MARK: Properties
    @IBOutlet weak var saveButton: UIBarButtonItem!

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var streetNameTextField: UITextField!
    @IBOutlet weak var buildingNameTextField: UITextField!
    @IBOutlet weak var floorNameTextField: UITextField!
    @IBOutlet weak var flatNumberTextField: UITextField!
    
    @IBOutlet weak var reminderImage: UIImageView!
    @IBOutlet weak var binImageView: UIImageView!
    @IBOutlet weak var blackBinButton: UIButton!
    @IBOutlet weak var whiteBinButton: UIButton!
    @IBOutlet weak var greenBinButton: UIButton!
    
    @IBOutlet var checkButton: UIButton!
    
//    var is_notification = ""
    
    
    var paymentType = String()
    var formatter = NumberFormatter()
//    private let customerContext: STPCustomerContext
//    private let paymentContext: STPPaymentContext

    
    //    - street
//    - building name
//    - floor
//    - flat
    

    var meal: Meal?
    var check = false
    var sw = false
    var list = ["藍田匯景花園（逢星期一回收）","藍田麗港城二期（逢星期二回收)","牛頭角樂華邨（逢星期三回收）"]
    var streetNameModelArray = [StreetList]()
    var selectedStreet = StreetList()
    
    // MARK:: viewDidLoad
    
    
//    required init?(coder aDecoder: NSCoder) {
//        customerContext = STPCustomerContext(keyProvider: MainAPIClient.shared)
//        paymentContext = STPPaymentContext(customerContext: customerContext)
//        super.init(coder: aDecoder)
//        paymentContext.delegate = self
//        paymentContext.hostViewController = self
//    }

  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatter.generatesDecimalNumbers = true

        
        if let number = UserDataManager.phoneNumber() {
            self.phoneNumberTextField.text = number
        }
        
        //Asked for permission
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
        })
        
        if let email = (UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as! NSDictionary)["email"]{
            emailTextField.text = (email as! String)
            //emailTextField.textColor = .lightGray
          //  emailTextField.isUserInteractionEnabled = false
        }
        
        // Enable the Save button only if the text field has a valid Meal name.
        // Disable the Save button while editing.
        saveButton.isEnabled = false
        
        self.binButtonsAction(self.blackBinButton)
        self.setUpInputPicker()
        self.getStreetNamesAPI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.navigationController?.navigationBar.tintColor =  #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
//        self.navigationController?.view.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
//        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = UIColor.green
        //setStatusBarBackgroundColor(color: .green)
        //UINavigationBar.appearance().barStyle = .blackOpaque
        
        //avigationController?.navigationBar.barTintColor = UIColor.green
        IQKeyboardManager.shared.enable = true

    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        IQKeyboardManager.shared.enable = false
        //setStatusBarBackgroundColor(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
    }
    
    
    func setUpInputPicker() {
        let streetNamePickerView = UIPickerView()
        streetNamePickerView.delegate = self
        streetNamePickerView.dataSource = self
        self.streetNameTextField.isUserInteractionEnabled = true
        streetNameTextField.inputView = streetNamePickerView
    }
    
    
    
    @IBAction func setNotificationButtonAction(_ sender: UIButton){
        
        let destinationVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
       
//        if !self.is_notification.isEmpty {
//            let arrayDays = self.is_notification.components(separatedBy: ",")
//            let mutableArray = NSMutableArray(array: arrayDays)
//            destinationVc.selectedDaysArray = mutableArray
//        }
//
//        destinationVc.notificationAddedCompletionBlock = { isNotifiationOn in
//            print(isNotifiationOn)
//            self.is_notification = isNotifiationOn
//        }
        
        self.navigationController?.pushViewController(destinationVc, animated: true)
    }
    
    @IBAction func binButtonsAction(_ sender: UIButton) {
       
        whiteBinButton.isSelected = false
        greenBinButton.isSelected = false
        blackBinButton.isSelected = false
        sender.isSelected = true
     
        let image = sender.image(for: .normal)
        self.binImageView.image = image
    }

    
    //MARK: T&C Check Button
    @IBAction func checkBoxButton(_ sender: UIButton) {
        
        
//        self.presentPaymentMethodsViewController()
      
//        buttonChangeImage(button: sender, onImage: #imageLiteral(resourceName: "CheckBox"), offImage: #imageLiteral(resourceName: "UnCheckBox"))
        
        self.view.endEditing(true)
        
        sender.isSelected = !sender.isSelected
       
        if check == false{
            updateSaveButtonState()
            check = true
        }
        else{
            saveButton.isEnabled = false
            check = false
        }
        
        self.enableSaveButton()
        
    }
        

    func buttonChangeImage (button:UIButton, onImage: UIImage, offImage: UIImage){
        if button.currentImage == offImage {
            button.setImage(onImage, for: .normal)
        } else {
            button.setImage(offImage, for: .normal)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    //MARK: Navigation
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
     
        dismiss(animated: true, completion: nil)
    }
    
    func showInvalidEmailWarning() {
        
        //Create the alert controller and actions
//        let alert = UIAlertController(title: "无效的邮件地址", message: "请检查您的电子邮件地址", preferredStyle: .alert)
        let alert = UIAlertController(title: "無效的電郵地址", message: "請檢查您的電郵地址", preferredStyle: .alert)

//        let deleteAction = UIAlertAction(title: "好的", style: .destructive) { _ in
//        }
        let deleteAction = UIAlertAction(title: "好", style: .destructive) { _ in
        }

        
        //Add the actions to the alert controller
        alert.addAction(deleteAction)
        //Present the alert controller
        present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: Helpers
    
//    private func presentPaymentMethodsViewController() {
//
//        guard !STPPaymentConfiguration.shared().publishableKey.isEmpty else {
//            // Present error immediately because publishable key needs to be set
//            let message = "Please assign a value to `publishableKey` before continuing. See `AppDelegate.swift`."
//            present(UIAlertController(message: message), animated: true)
//            return
//        }
//
//        guard !MainAPIClient.shared.baseURLString.isEmpty else {
//            // Present error immediately because base url needs to be set
//            let message = "Please assign a value to `MainAPIClient.shared.baseURLString` before continuing. See `AppDelegate.swift`."
//            present(UIAlertController(message: message), animated: true)
//            return
//        }
//
//        // Present the Stripe payment methods view controller to enter payment details
//        paymentContext.presentPaymentOptionsViewController()
//    }

    
//    func paymentOptionsAlert() {
//
//        //Create the alert controller and actions
//        let alert = UIAlertController(title: "Pay Using!!", message: "Choose your payment option.", preferredStyle: .alert)
//
//        let applePayAction = UIAlertAction(title: "Apple Pay", style: .default) { _ in
//            self.didPressApplePay()
//        }
//
//        let cardPayAction = UIAlertAction(title: "Use Card", style: .default) { _ in
//            self.payWithCard()
//        }
//
//        let cancel = UIAlertAction.init(title: "Cancel", style: .destructive, handler: nil)
//
//        alert.addAction(cardPayAction)
//        alert.addAction(applePayAction)
//        alert.addAction(cancel)
//        present(alert, animated: true, completion: nil)
//    }


    
    
    @IBAction func saveOrderButtonAction(_ sender: Any) {
        
        let emailTextStr = emailTextField.text?.removeEmptySpaces() ?? ""
        
        if emailTextStr.isEmpty {
            self.performSegue(withIdentifier: "SubscriptionVc", sender: self)
        }else{
            if !emailTextStr.isValidEmail() {
                self.showInvalidEmailWarning()
            }else{
                self.performSegue(withIdentifier: "SubscriptionVc", sender: self)
            }
        }
        

//        self.getUserProfileAPI()
    }
    
    
    
    
    func placeOrder(subscription_id:String,paymentMode:String)  {
        
        let userName = nameTextField.text?.removeEmptySpaces() ?? ""
        let phoneNumberText = phoneNumberTextField.text?.removeEmptySpaces() ?? ""
        let emailText = emailTextField.text?.removeEmptySpaces() ?? ""
        let buildingName = buildingNameTextField.text?.removeEmptySpaces() ?? ""
        let floorName = floorNameTextField.text?.removeEmptySpaces() ?? ""
        let flatNumber = flatNumberTextField.text?.removeEmptySpaces() ?? ""
        
        guard let userId = UserDataManager.userId() else { return }
        
        
        if !emailText.isEmpty {
            if !emailText.isValidEmail() {
                self.showInvalidEmailWarning()
                return
            }
        }
        
//        "is_notification":is_notification,

        let dicParams = ["user_id":userId,
                         "recycle_bin_id":self.getBinId(),
                         "user_name":userName,
                         "phone_number":phoneNumberText,
                         "user_email":emailText,
                         "street_number":self.getStreetId(),
                         "building_name":buildingName,
                         "floor_number":floorName,
                         "flat_number":flatNumber,
                         "subscription_id":subscription_id,
                         "payment_mode":paymentMode] as [String : Any]
        
        
        print(dicParams)
        
        self.placeOrderAPI(params: dicParams)

    }
    
    func getBinId() -> Int  {
        
        if self.blackBinButton.isSelected {
            return 1
        }else if self.whiteBinButton.isSelected{
            return 2
        }else{
            return 3
        }
    }
    
    func getStreetId() -> Int {
        guard let streetId = self.selectedStreet.id else {
            self.saveButton.isEnabled = false
            return 0 }
        return streetId
    }
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "SubscriptionVc" {
            let destinationVc = segue.destination as! SubscriptionVc
            destinationVc.completionSubscriptionDone = {  subscription_id,paymentMode in
               
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.placeOrder(subscription_id:subscription_id, paymentMode: paymentMode)
                }
            }
        }
    }
    
    //MARK: Actions
    
    //MARK: Private Methods
    
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty.
        
        let userName = nameTextField.text?.removeEmptySpaces() ?? ""
        saveButton.isEnabled = !userName.isEmpty
        
        let phoneNumberText = phoneNumberTextField.text?.removeEmptySpaces() ?? ""
        saveButton.isEnabled = !phoneNumberText.isEmpty
        
        let streetName = streetNameTextField.text?.removeEmptySpaces() ?? ""
        saveButton.isEnabled = !streetName.isEmpty
        
        let buildingName = buildingNameTextField.text?.removeEmptySpaces() ?? ""
        saveButton.isEnabled = !buildingName.isEmpty
        
        let floorName = floorNameTextField.text?.removeEmptySpaces() ?? ""
        saveButton.isEnabled = !floorName.isEmpty
        
        let flatNumber = flatNumberTextField.text?.removeEmptySpaces() ?? ""
        saveButton.isEnabled = !flatNumber.isEmpty
        
    }
    
   
    func enableSaveButton()  {
        
        let userName = nameTextField.text?.removeEmptySpaces() ?? ""
        let phoneNumberText = phoneNumberTextField.text?.removeEmptySpaces() ?? ""
//        let emailTextStr = emailTextField.text?.removeEmptySpaces() ?? ""
        let streetName = streetNameTextField.text?.removeEmptySpaces() ?? ""
        let buildingName = buildingNameTextField.text?.removeEmptySpaces() ?? ""
        let floorName = floorNameTextField.text?.removeEmptySpaces() ?? ""
        let flatNumber = flatNumberTextField.text?.removeEmptySpaces() ?? ""

        if userName.isEmpty || phoneNumberText.isEmpty || streetName.isEmpty || buildingName.isEmpty || floorName.isEmpty || flatNumber.isEmpty || !self.checkButton.isSelected {
           
            self.saveButton.isEnabled = false
        }else{
            
            self.saveButton.isEnabled = true

//            if !emailTextStr.isValidEmail(){
//                self.saveButton.isEnabled = false
//            }else{
//                self.saveButton.isEnabled = true
//            }
        }
        
    }
    
    
    func getStreetNamesAPI() {
        
        APIManager.shared.getDataFromServer_OneLineParsing(GET_STREET_NAME_URL) { (data, success) in
            
            if success{
                do{
                    let streetDataReponse = try JSONDecoder().decode(StreetData.self, from: data)
                    print(streetDataReponse.message)
                    self.streetNameModelArray = streetDataReponse.data
                }
                catch let jsonErr {
                    print("error",jsonErr)
                }
            }else{
                print("no streets")
            }
        }
    }

    func placeOrderAPI(params:[String : Any])  {
        
        print(params)
        
        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: PLACE_ORDER_URL) { (response, success) in
            
            IndicatorManager.stopIndicator()

            print(response)
            
            if success {
               
                let message = response.value(forKey: "message") as! String
                IndicatorManager.showTSMessage(message: message, type: .success)
               
                DispatchQueue.main.async {
                   
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func getUserProfileAPI() {
        
        guard let userId = UserDataManager.userId() else { return }
        
        let params = ["user_id":userId]
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: GET_USER_PROFILE_URL) { (resultDic, success) in
            
            if success{
                
                let userData = resultDic.value(forKey: "data")
                UserDataManager.saveData(userDic: userData as! [String:AnyObject])
                
                
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when, execute: {
//                    if UserDataManager.isSubscribed() {
//                        self.placeOrder()
//                    }else{
//                        self.performSegue(withIdentifier: "SubscriptionVc", sender: self)
//                    }
                })


            }
        }
    }
    
    @IBAction func termsGestureAction(_ sender: Any) {
        performSegue(withIdentifier: "B", sender: nil)
    }
    
    func showLinksClicked() {
        let safariVC = SFSafariViewController(url: URL(string: "https://www.google.co.in")!)
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension MealViewController: UITextFieldDelegate{
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.streetNameTextField {
            if streetNameModelArray.count == 0{
                return false
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // Disable the Save button while editing.
        saveButton.isEnabled = false

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        // textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        updateSaveButtonState()
      
        self.enableSaveButton()
    }

}

extension MealViewController:UIPickerViewDelegate, UIPickerViewDataSource{
  
    //MARK: Drop Down Menu List for Address
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.streetNameModelArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.streetNameModelArray[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = self.streetNameModelArray[row].name
        let myTitle = NSAttributedString(string: titleData!, attributes: [NSAttributedStringKey.font:UIFont(name: "Times", size: 100.0)!,NSAttributedStringKey.foregroundColor:UIColor.black])
        
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        self.streetNameTextField.text = self.streetNameModelArray[row].name
        self.selectedStreet = self.streetNameModelArray[row]
        self.streetNameTextField.resignFirstResponder()
    }
    
    

}

import Stripe

//extension MealViewController: STPAddCardViewControllerDelegate,
//PKPaymentAuthorizationViewControllerDelegate{
//
//
//    func decimal(with string: String) -> NSDecimalNumber {
//        return formatter.number(from: string) as? NSDecimalNumber ?? 0
//    }
//
//
//    func didPressApplePay() {
//
//        paymentType = "apple_pay"
//
//        let merchantIdentifier = "merchant.theloophk"
//        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "HK", currency: "HKD")
//
//        let productSummery = PKPaymentSummaryItem.init(label: "Subscription", amount: 10)
//
//        paymentRequest.paymentSummaryItems = [productSummery]
//
//
//        if Stripe.canSubmitPaymentRequest(paymentRequest) {
//            // Setup payment authorization view controller
//            let paymentAuthorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
//            paymentAuthorizationViewController!.delegate = self
//
//            // Present payment authorization view controller
//            present(paymentAuthorizationViewController!, animated: true)
//        }
//        else {
////            UtilityClass.shared.showTSMessage(message: "You do not have any card in your wallet.Please select another payment method.", type: "warning")
//        }
//    }
//
//
//    // MARK: stripe methods
//
//    func payWithCard(){
//
//        paymentType = "card"
//
//        let addCardViewController = STPAddCardViewController()
//        addCardViewController.delegate = self
//        // Present add card view controller
//        let navigationController = UINavigationController(rootViewController: addCardViewController)
//        present(navigationController, animated: true)
//    }
//
//
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        // Dismiss add card view controller
//        dismiss(animated: true)
//    }
//
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
//
//
//
//        self.subscriptionAPI(token: token.tokenId)
//
//        print(token.tokenId)
//
//    }
//
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
//
//
//        STPAPIClient.shared().createToken(with: payment) { (token: STPToken?, error: Error?) in
//
//            guard let token = token, error == nil else {
////                UtilityClass.shared.showTSMessage(message: error!.localizedDescription, type: "error")
//                return
//            }
//
//            self.subscriptionAPI(token: token.tokenId)
//
//            print(token.tokenId)
//
//        }
//    }
//
//    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        // Dismiss payment authorization view controller
//
//        dismiss(animated: true, completion: {
//            if (true) {
//                // Show a receipt page...
//            }
//        })
//    }
//
//
//    func subscriptionAPI(token:String)  {
//
////        user_id:1
////        name:Test user
////        stripe_token:tok_mastercard
//
//        guard let userId = UserDataManager.userId() else { return  }
//
//        let params = ["user_id":userId,"name":self.nameTextField.text!,"stripe_token":token] as [String : Any]
//
//        APIManager.shared.postDataToServer(params as [String : AnyObject], url: SUBSCRIPTION_URL) { (responseDic, success) in
//
//            print(responseDic)
//
//        }
//
//    }
//
//
//}


//extension MealViewController: STPPaymentContextDelegate{
//
//    // MARK: STPPaymentContextDelegate
//
//    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
//
//
//        if let customerKeyError = error as? MainAPIClient.CustomerKeyError {
//            switch customerKeyError {
//            case .missingBaseURL:
//                // Fail silently until base url string is set
//                print("[ERROR]: Please assign a value to `MainAPIClient.shared.baseURLString` before continuing. See `AppDelegate.swift`.")
//            case .invalidResponse:
//                // Use customer key specific error message
//                print("[ERROR]: Missing or malformed response when attempting to `MainAPIClient.shared.createCustomerKey`. Please check internet connection and backend response formatting.");
//
//
//                present(UIAlertController(message: "Could not retrieve customer information", retryHandler: { (action) in
//                    // Retry payment context loading
//                    paymentContext.retryLoading()
//                }), animated: true)
//
//
//
//            }
//        }
//        else {
//            // Use generic error message
//            print("[ERROR]: Unrecognized error while loading payment context: \(error)");
//
//
//            self.present(UIAlertController(message: "Could not retrieve payment information", retryHandler: { (action) in
//                // Retry payment context loading
//                paymentContext.retryLoading()
//            }), animated: true)
//
//
//        }
//    }
//
//    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
//        // Reload related components
//
//        print("paymentContextDidChange")
//
////         self.paymentContext.requestPayment()
//
////         reloadPaymentButtonContent()
////         reloadRequestRideButton()
//    }
//
//    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {
//        // Create charge using payment result
//
//
//        let source = paymentResult.source.stripeID
//
//
//        print(source)
//
//
////        MainAPIClient.shared.requestRide(source: source, amount: price, currency: "usd") { [weak self] (ride, error) in
////            guard let strongSelf = self else {
////                // View controller was deallocated
////                return
////            }
////
////            guard error == nil else {
////                // Error while requesting ride
////                completion(error)
////                return
////            }
////
////            // Save ride info to display after payment finished
////            strongSelf.rideRequestState = .active(ride!)
////            completion(nil)
////        }
//
//    }
//
//    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
//        switch status {
//
//        case .success:
//            // Animate active ride
////            animateActiveRide()
//            break
//        case .error:
//            // Present error to user
//
//            if let requestRideError = error as? MainAPIClient.RequestRideError {
//                switch requestRideError {
//                case .missingBaseURL:
//                    // Fail silently until base url string is set
//                    print("[ERROR]: Please assign a value to `MainAPIClient.shared.baseURLString` before continuing. See `AppDelegate.swift`.")
//                case .invalidResponse:
//                    // Missing response from backend
//                    print("[ERROR]: Missing or malformed response when attempting to `MainAPIClient.shared.requestRide`. Please check internet connection and backend response formatting.");
//                    present(UIAlertController(message: "Could not request ride"), animated: true)
//                }
//            }
//            else {
//                // Use generic error message
//                print("[ERROR]: Unrecognized error while finishing payment: \(String(describing: error))");
//                present(UIAlertController(message: "Could not request ride"), animated: true)
//            }
//
//        case .userCancellation:
//            // Reset ride request state
//            break
//        }
//    }
//
//}
