//
//  MealTableViewCell.swift
//  Loop
//
//  Created by Jorch Wong on 31/12/2018.
//  Copyright © 2018 Jorch Wong. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet weak var buildingName: UILabel!
    @IBOutlet weak var floorName: UILabel!
    @IBOutlet weak var flatName: UILabel!
    @IBOutlet weak var reminderImage: UIImageView!
    @IBOutlet weak var binImageView: UIImageView!
    @IBOutlet weak var canceledLabel: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
