//
//  SubscriptionVc.swift
//  Loop
//
//  Created by KHOSA on 16/07/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import Stripe

class SubscriptionVc: UIViewController {
    
    
    @IBOutlet var paymentButton: UIButton!
    @IBOutlet var buySubscriptionButton: UIButton!
    
    var completionSubscriptionDone:((String,String)->Void)?
    
    private let customerContext: STPCustomerContext
    private let paymentContext: STPPaymentContext
    
    
    
    required init?(coder aDecoder: NSCoder) {
        self.customerContext = STPCustomerContext(keyProvider: MainAPIClient.shared)
        self.paymentContext = STPPaymentContext(customerContext: customerContext)
        
        super.init(coder: aDecoder)
        self.paymentContext.delegate = self
        self.paymentContext.hostViewController = self
        self.paymentContext.paymentCurrency = "hkd"
        self.paymentContext.paymentAmount = 999 // This is in cents, i.e. $50 USD
        
        // self.paymentContext.presentPaymentOptionsViewController()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        // self.navigationController?.view.backgroundColor = .clear
        setNavigationBarClear(vc: self)
    }
    
    private func reloadPaymentButtonContent() {
        
        guard let selectedPaymentMethod = paymentContext.selectedPaymentOption else {
            // Show default image, text, and color
            paymentButton.setImage(#imageLiteral(resourceName: "Payment"), for: .normal)
            paymentButton.setTitle("付款", for: .normal)
            paymentButton.setTitleColor(.gray, for: .normal)
            return
        }
        
        // Show selected payment method image, label, and darker color
        paymentButton.setImage(selectedPaymentMethod.image, for: .normal)
        paymentButton.setTitle(selectedPaymentMethod.label, for: .normal)
        paymentButton.setTitleColor(.blue, for: .normal)
    }
    
    private func reloadBuyButton(){
        if paymentContext.selectedPaymentOption == nil {
            //            buySubscriptionButton.backgroundColor = .loopGrayColor
            //            buySubscriptionButton.setTitle("订阅", for: .normal)
            //            buySubscriptionButton.setTitleColor(.white, for: .normal)
            buySubscriptionButton.isEnabled = false
        }else{
            buySubscriptionButton.isEnabled = true
            //            buySubscriptionButton.backgroundColor = .loopGreenColor
            //            buySubscriptionButton.setTitle("订阅", for: .normal)
            //            buySubscriptionButton.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBAction private func handlePaymentButtonAction() {
        presentPaymentMethodsViewController()
    }
    
    @IBAction func buySubscriptionButtonAction() {
        self.paymentContext.requestPayment()
    }
    
    
    // MARK: Helpers
    private func presentPaymentMethodsViewController() {
        
        guard !STPPaymentConfiguration.shared().publishableKey.isEmpty else {
            // Present error immediately because publishable key needs to be set
            let message = "Please assign a value to `publishableKey` before continuing. See `AppDelegate.swift`."
            present(UIAlertController(message: message), animated: true)
            return
        }
        
        guard !MainAPIClient.shared.baseURLString.isEmpty else {
            // Present error immediately because base url needs to be set
            let message = "Please assign a value to `MainAPIClient.shared.baseURLString` before continuing. See `AppDelegate.swift`."
            present(UIAlertController(message: message), animated: true)
            return
        }
        
        print(STPPaymentConfiguration.shared().publishableKey)
        print(MainAPIClient.shared.baseURLString)
        //for 
        // Present the Stripe payment methods view controller to enter payment details
        // paymentContext.presentPaymentOptionsViewController()
        //self.paymentContext.delegate = self
        //self.paymentContext.requestPayment()
        self.paymentContext.pushPaymentOptionsViewController()
        print(self.paymentContext.paymentOptions)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension SubscriptionVc: STPPaymentContextDelegate{
    
    // MARK: STPPaymentContextDelegate
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        if let customerKeyError = error as? MainAPIClient.CustomerKeyError {
            switch customerKeyError {
            case .missingBaseURL:
                // Fail silently until base url string is set
                print("[ERROR]: Please assign a value to `MainAPIClient.shared.baseURLString` before continuing. See `AppDelegate.swift`.")
            case .invalidResponse:
                // Use customer key specific error message
                print("[ERROR]: Missing or malformed response when attempting to `MainAPIClient.shared.createCustomerKey`. Please check internet connection and backend response formatting.");
                
                
                present(UIAlertController(message: "Could not retrieve customer information", retryHandler: { (action) in
                    // Retry payment context loading
                    paymentContext.retryLoading()
                }), animated: true)
                
                
                
            }
        }
        else {
            // Use generic error message
            print("[ERROR]: Unrecognized error while loading payment context: \(error)");
            
            
            self.present(UIAlertController(message: "Could not retrieve payment information", retryHandler: { (action) in
                // Retry payment context loading
                paymentContext.retryLoading()
            }), animated: true)
        }
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        // Reload related components
        
        print("paymentContextDidChange")
        reloadPaymentButtonContent()
        reloadBuyButton()
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {
        // Create charge using payment result
        
        print(paymentContext.selectedPaymentOption?.label as Any)
        
        let source = paymentResult.source.stripeID
        
         self.subscriptionAPI(token: source,paymentMode: paymentContext.selectedPaymentOption!.label)
       // self.subcriptionapiurlsession(token: source,paymentMode: paymentContext.selectedPaymentOption!.label)
        
        completion(nil)
        
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        switch status {
            
            
        case .success:
            // Animate active ride
            
            print(".success")
            
            // animateActiveRide()
            break
        case .error:
            // Present error to user
            
            if let paymentError = error as? MainAPIClient.RequestRideError {
                switch paymentError {
                case .missingBaseURL:
                    // Fail silently until base url string is set
                    print("[ERROR]: Please assign a value to `MainAPIClient.shared.baseURLString` before continuing. See `AppDelegate.swift`.")
                case .invalidResponse:
                    // Missing response from backend
                    print("[ERROR]: Missing or malformed response when attempting to `MainAPIClient.shared.requestRide`. Please check internet connection and backend response formatting.");
                    
                    present(UIAlertController(message:  paymentError.localizedDescription), animated: true)
                }
            }
            else {
                // Use generic error message
                print("[ERROR]: Unrecognized error while finishing payment: \(String(describing: error))");
                present(UIAlertController(message: error?.localizedDescription), animated: true)
            }
            
        case .userCancellation:
            // Reset ride request state
            break
        }
    }
    
    
    func subscriptionAPI(token:String,paymentMode:String)  {
        
        
        // guard let customerId = UserDataManager.customer_Id() else { return  }
        if let customer_id = (UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as! NSDictionary)["customer_id"]{
            //cus_FRhHw7Kg8Fiy29
            let params = ["customer_id":customer_id,"stripe_token":token] as [String : Any]
            
            print(params)
            
            IndicatorManager.startIndicator()
            
            APIManager.shared.postDataToServer(params as [String : AnyObject], url: SUBSCRIPTION_URL) { (response, success) in
                
                print(response)
                
                IndicatorManager.stopIndicator()
                
                if success{
                    let userData = response.value(forKey: "data")
                    UserDataManager.saveData(userDic: userData as! [String:AnyObject])
                    let subscription_id = response.value(forKey: "subscription_id") as! String
                    self.navigationController?.popViewController(animated: true)
                    self.completionSubscriptionDone!(subscription_id, paymentMode)
                }else{
                    print(response)
                }
            }
        }
        
    }
    
    func subcriptionapiurlsession(token:String,paymentMode:String){
        
        let urlStr = "http://theloopshk.com:3000/api/payment"
        
        if let url = URL(string: urlStr){
            var request = URLRequest(url: url)
            guard let customer_id = (UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as! NSDictionary)["customer_id"] else {return}
           let cusid  = "\(customer_id)"
            let stpToken = "\(token)"
            let params = ["customer_id":cusid,"stripe_token":stpToken] as [String : String]
            print(params)
            request.httpMethod = "POST"
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                return
            }
            request.httpBody = httpBody
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    print(json)
                    
                    
                }catch {
                    print("In catch ")
                    print(error)
                }
                
            }
            task.resume()
        }else{
            DispatchQueue.main.async {
                //     LottieLoader.shared.hideLoader()
                //Alerts.shared.showError(errorMsg: "wrong Url request")
            }
        }
        
    }
}
