//
//  OrdersVc.swift
//  Loop
//
//  Created by KHOSA on 16/07/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
//import UserNotifications
import AccountKit
import IQKeyboardManagerSwift

class navigationBar: UINavigationController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        
        
    }
}

class OrdersVc: UIViewController {
    
    var ordersList = [OrderList]()
    var _accountKit = AccountKit(responseType: .accessToken)
    @IBOutlet var tableView: UITableView!
    
    var customView:EmptyTableView? = EmptyTableView.instanceFromNib()
    
    var emptyOrderView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAddOrderButton()
        //UserData.shared.getUserData()
        print((UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as? NSDictionary)!)
        //self.setUpCustomView()
    }
    
    
    func setUpCustomView()  {
        
        if customView == nil {
            customView = EmptyTableView.instanceFromNib()
        }
        
        customView!.button.addTarget(self, action: #selector(self.addOrderButtonAction(_:)), for: .touchUpInside)
        self.customiseButton(button: customView!.button)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setNavigationBarClear(vc: self)
        
        IQKeyboardManager.shared.enable = false
        setStatusBarBackgroundColor(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0))
        self.getOrdersListAPI()
    }
    
    
    func setupAddOrderButton(){
        emptyOrderView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 30, height: 85))
        emptyOrderView.center = self.view.center
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: emptyOrderView.frame.width, height: 25))
        
        label.text = "找不到訂單"
        label.textColor = .white
        label.textAlignment = .center
        emptyOrderView.addSubview(label)
        
        let button = UIButton(frame: CGRect(x: 0, y: label.frame.maxY + 10, width: emptyOrderView.frame.width, height: 50))
        //button.center = self.view.center
        button.setTitle("新增訂單", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 25
        button.borderWidth = 1
        button.borderColor = .white
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(self.addOrderButtonAction(_:)), for: .touchUpInside)
        emptyOrderView.addSubview(button)
        self.view.addSubview(emptyOrderView)
        
        
        
    }
    
    func hideNoOrder(){
        if ordersList.count > 0 {
            emptyOrderView.isHidden = true
        }else{
            emptyOrderView.isHidden = false
            
        }
    }
    
    
    
    func chooseBinImage(binId:String) -> UIImage {
        
        switch binId {
        case "1":
            return UIImage.init(named: "blackBinUnSelected")!
        case "2":
            return UIImage.init(named: "whiteBinUnSelected")!
        case "3":
            return UIImage.init(named: "greenBinUnSelected")!
        default:
            return UIImage.init(named: "blackBinUnSelected")!
        }
    }
    
    // MARK:: ALERT METHODS
    
    func showDeleteWarning(index:Int) {
        
        let alert = UIAlertController(title: "停止回收服務", message: "你確定要終止回收服務嗎？", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "否", style: .cancel, handler: nil)
        
        let deleteAction = UIAlertAction(title: "是", style: .destructive) { _ in
            DispatchQueue.main.async {
                self.cancelServiceAPI(index: index)
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        present(alert, animated: true, completion: nil)
    }
    
    func countryName(from countryCode: String) -> String {
        
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            return name
        } else {
            return countryCode
        }
    }
    
    @IBAction func addOrderButtonAction(_ sender: Any) {
        
        //        if UserDataManager.userData() == nil {
        //            self.loginWithPhone()
        //        }else{
        self.presentAddOrderScreen()
        //  }
    }
    
    
    func presentAddOrderScreen()  {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "AddOrderNavigation")
        
        self.present(destination!, animated: true, completion: nil)
    }
    
    
    
    // MARK:: API METHODS
    
    
    func signInUserAPI(params:[String:AnyObject])  {
        
        APIManager.shared.postDataToServer(params, url: SIGNUP_USER_URL) { (response, success) in
            
            IndicatorManager.stopIndicator()
            print(response)
            
            if success{
                
                let userData = response.value(forKey: "data")
                UserDataManager.saveData(userDic: userData as! [String:AnyObject])
                
                NotificationManager.shared.checkUserNotifications()
                self.hideNoOrder()
                    self.tableView.reloadData()
                self.presentAddOrderScreen()
            }
        }
    }
    
    
    func getOrdersListAPI()  {
        
        guard let userid = (UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as! NSDictionary)["id"] else {
            self.ordersList.removeAll()
             self.hideNoOrder()
                self.tableView.reloadData()
            return
        }
        
        
        let params = ["user_id":userid] //11
        
        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: GET_ORDERS) { (resultDic, success) in
            
            print(resultDic)
            
            if success{
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: resultDic, options: .prettyPrinted)
                    let streetDataReponse = try JSONDecoder().decode(UserOrdersData.self, from: data)
                    self.ordersList = streetDataReponse.data
                     self.hideNoOrder()
                    
                } catch let jsonErr {
                    print("Error serializing json: ", jsonErr)
                     self.hideNoOrder()
                }
            }else{
                
            }
             self.hideNoOrder()
            self.tableView.reloadData()
            
        }
    }
    
    
    
    func cancelServiceAPI(index:Int)  {
        
        let orderModel = self.ordersList[index]
        
        guard let subscription_id = orderModel.subscription_id else {
            return
        }
        
        let params = ["subscription_id":subscription_id]
        
        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: CANCEL_SUBSCRIPTION_URL) { (resultDic, success) in
            
            print(resultDic)
            
            if success{
                self.getOrdersListAPI()
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func customiseButton(button:UIButton)  {
        button.setTitle(self.buttonTitle(), for: .normal)
        button.addTarget(self, action: #selector(self.addOrderButtonAction(_:)), for: .touchUpInside)
    }
    
    func buttonTitle() -> String {
        
        if UserDataManager.userId() != nil {
            customView?.phoneIconImageView.isHidden = true
            return " 建立回收桶 "
        }else{
            customView?.phoneIconImageView.isHidden = false
            return " 電話號碼登入 "
        }
    }
    
    func setTableBackground()  {
        
        //self.setUpCustomView()
        // tableView.backgroundView = customView
        
        //        if UserDataManager.userId() != nil {
        //            tableView.backgroundView = nil
        //        }else{
        //            tableView.backgroundView = customView
        //        }
    }
    
}

extension OrdersVc: UITableViewDelegate,UITableViewDataSource {
    
    // MARK:: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.ordersList.count > 0{
            tableView.backgroundView = nil
            
            return self.ordersList.count
        }else{
            self.setTableBackground()
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "MealTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MealTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        
        let orderModel = self.ordersList[indexPath.row]
        
        cell.canceledLabel.isHidden = orderModel.is_cancelled == 1 ? false:true
        cell.nameLabel.text = orderModel.user_name
        cell.phoneNumberLabel.text = orderModel.phone_number
        cell.streetName.text = orderModel.street_name
        cell.buildingName.text = orderModel.building_name
        cell.floorName.text = orderModel.floor_number
        cell.flatName.text = orderModel.flat_number
        cell.binImageView.image = self.chooseBinImage(binId: orderModel.recycle_bin_id!)
        return cell
    }
    
    
    
    // MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let orderModel = self.ordersList[indexPath.row]
        let is_cancelled = Bool(truncating: orderModel.is_cancelled as NSNumber)
        return !is_cancelled
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            
            DispatchQueue.main.async {
                self.showDeleteWarning(index: indexPath.row)
            }
        }
    }
}


// MARK:: AKFViewControllerDelegate Facebook Account kit for login
extension OrdersVc: AKFViewControllerDelegate {
    
    func loginWithPhone(){
        
        let viewController = _accountKit.viewControllerForPhoneLogin()
        viewController.isSendToFacebookEnabled = true
        viewController.isGetACallEnabled = true
        viewController.isInitialSMSButtonEnabled = true
        self.prepareLoginViewController(loginViewController: viewController)
        
        self.present(viewController as UIViewController, animated: true, completion: nil)
    }
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        
        loginViewController.delegate = self
        
        
        let backImage = UIImage.init(named: "background1")
        
        let color = UIColor.init(rgb: 0x000000)
        
        let uiManager = SkinManager.init(skinType: .contemporary, primaryColor: color, backgroundImage: backImage, backgroundTint: .white, tintIntensity: 0.60)
        
        var preferredStatusBarStyle: UIStatusBarStyle {
            return.default
        }
        
        let theme:Theme = Theme.default()
        theme.headerBackgroundColor = color
        theme.contentHeaderLayoutWeight = 64
        //        theme.headerTextColor = UIColor.white
        //        theme.headerTextType = .login
        theme.inputTextColor = UIColor.black.withAlphaComponent(0.87)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor.black.withAlphaComponent(0.87)
        
        loginViewController.setTheme(theme)
        
        loginViewController.uiManager = uiManager
    }
    
    
    func getAccountDetail()  {
        
        IndicatorManager.startIndicator()
        
        _accountKit.requestAccount{
            (account, error) -> Void in
            
            if error != nil{
                IndicatorManager.stopIndicator()
                
            }else if account?.phoneNumber?.phoneNumber != nil {
                
                let mobile_no = account?.phoneNumber?.phoneNumber
                let country_code = account?.phoneNumber?.countryCode
                let country_name = self.countryName(from: country_code!)
                let account_id = account?.accountID
                
                
                let params:[String:String] = ["phone_number":mobile_no!,
                                              "country_code":country_code!,
                                              "country_name":country_name,
                                              "os_name":os_Name,
                                              "os_version":os_Version,
                                              "device_manufacture":device_Manufacture,
                                              "device_model":device_Model,
                                              "device_token":device_token,
                                              "model_number":model_No,
                                              "account_id":account_id!]
                
                print(params)
                
                self.signInUserAPI(params: params as [String : AnyObject])
            }
        }
    }
    
    // MARK: AKFViewControllerDelegate
    private func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AccessToken!, state: String!) {
        
        print("did complete login with access token \(accessToken.tokenString) state \(String(describing: state))")
    }
    
    // handle callback on successful login to show authorization code
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith code: String, state: String) {
        print("didCompleteLoginWithAuthorizationCode")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)) {
        // ... handle user cancellation of the login process ...
        print("viewControllerDidCancel")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didFailWithError error: Error) {
        // ... implement appropriate error handling ...
        print("\(String(describing: viewController)) did fail with error: \(error.localizedDescription)")
    }
    
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AccessToken, state: String) {
        
        print("didCompleteLoginWith")
        print(state as Any)
        
        self.getAccountDetail()
    }
}

