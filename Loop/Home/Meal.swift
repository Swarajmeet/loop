//
//  Meal.swift
//  Loop
//
//  Created by Jorch Wong on 31/12/2018.
//  Copyright © 2018 Jorch Wong. All rights reserved.
//

import UIKit


class Meal {
    
    //MARK: Properties
    
    var name: String
    var phoneNumber: String
    var address: String
    var address2: String
    var address3: String
    var address4: String
    var reminder: UIImage?
    var bin: UIImage?
    
    
    //MARK: Initialization
    
    init?(name: String, phoneNumber: String, address: String, address2: String, address3: String, address4: String, reminder: UIImage?, bin: UIImage?) {
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }

        // The phone number must not be empty
        guard !phoneNumber.isEmpty else {
            return nil
        }

        // The aaddress must not be empty
        guard !address.isEmpty else {
            return nil
        }
        guard !address2.isEmpty else {
            return nil
        }
        guard !address3.isEmpty else {
            return nil
        }
        guard !address4.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        self.phoneNumber = phoneNumber
        self.address = address
        self.address2 = address2
        self.address3 = address3
        self.address4 = address4
        self.reminder = reminder
        self.bin = bin
    }
    
}
