//
//  MainAPIClient.swift
//  RocketRides
//
//  Created by Romain Huet on 5/26/16.
//  Copyright © 2016 Romain Huet. All rights reserved.
//

import Alamofire
import Stripe

class MainAPIClient: NSObject, STPCustomerEphemeralKeyProvider {
    
    static let shared = MainAPIClient()
    
    var baseURLString = BASE_URL
    
    
    enum RequestRideError: Error {
        case missingBaseURL
        case invalidResponse
    }
    
    
    // MARK: STPEphemeralKeyProvider
    
    enum CustomerKeyError: Error {
        case missingBaseURL
        case invalidResponse
    }
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        
        
        //        guard let customer_id = UserDataManager.customer_Id() else {
        //            completion(nil, CustomerKeyError.missingBaseURL)
        //            return
        //        }
        
        if let customer_id = (UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as! NSDictionary)["customer_id"]{
            
          //cus_FRhHw7Kg8Fiy29
            let parameters: [String: Any] = ["api_version": apiVersion,"customer_id":customer_id]
            
            
            print(parameters)
            
            Alamofire.request(CREATE_EPHEMERAL_KEY_URL, method: .post, parameters: parameters).responseJSON { (response) in
                
                print(CREATE_EPHEMERAL_KEY_URL)
                guard let json = response.result.value as? [AnyHashable: Any] else {
                    print(CustomerKeyError.invalidResponse)
                    completion(nil, CustomerKeyError.invalidResponse)
                    return
                }
                
                
                print(json as Any)
                
                print(json.description)
                
                completion(json, nil)
            }
        }else{
            print(CustomerKeyError.missingBaseURL.localizedDescription)
            print("Customer Id Nil")
        }
        
        
        
    }
    
}
