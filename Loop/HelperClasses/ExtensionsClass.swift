//
//  ExtensionsClass.swift
//  Loop
//
//  Created by KHOSA on 10/07/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit


extension UIView
{
    /**
     * Rounds only corners top left, top right and bottom left of self.
     *
     * - parameter value: The radius to use for rounding the corners.
     */
    func roundCornersForOutBubbleContent(_ value: CGFloat){
        
        let corners: UIRectCorner = [.topLeft, .topRight, .bottomLeft]
        let cornerRadii = CGSize(width: value, height: value)
        
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: cornerRadii).cgPath
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = path
        
        self.layer.mask = rectShape
    }
    
    /**
     * Rounds only corners top left, top right and bottom right of self.
     *
     * - parameter value: The radius to use for rounding the corners.
     */
    func roundCornersForInBubbleContent(_ value: CGFloat)
    {
        let corners: UIRectCorner = [.topLeft, .topRight, .bottomRight]
        let cornerRadii = CGSize(width: value, height: value)
        
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: cornerRadii).cgPath
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = path
        
        self.layer.mask = rectShape
    }
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
    
    
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    
    
    func dropShadow(color:UIColor,
                    opacity:Float = 0.5,
                    offSet:CGSize,
                    radius:CGFloat = 1,
                    scale:Bool = true) {
        
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        print(self.bounds)
        
        
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
    }
    
    
    
    func applyShadow(color: UIColor = .black,
                     alpha: Float = 0.5,
                     x: CGFloat = 0,
                     y: CGFloat = 2,
                     blur: CGFloat = 4,
                     spread: CGFloat = 0)  {
        
        
        //change it to .height if you need spread for height
        
        let radius: CGFloat = layer.frame.width / 2.0
        
        print(bounds)
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: spread * radius, height: layer.frame.height))
        layer.shadowOffset = CGSize.init(width: x, height: y)
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = alpha
        layer.shadowRadius = blur
        layer.masksToBounds = false
        layer.shadowPath = shadowPath.cgPath
        
    }
    
    
    
    func drawStroke(width: CGFloat, color: UIColor) {
        
        
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height), cornerRadius: layer.cornerRadius)
        
        let shapeLayer = CAShapeLayer ()
        
        shapeLayer.path = path.cgPath
        
        shapeLayer.fillColor = UIColor.clear.cgColor
        
        shapeLayer.strokeColor = color.cgColor
        
        shapeLayer.lineWidth = width
        
        self.layer.addSublayer(shapeLayer)
    }
    
}

extension String {
    
    func isValidEmail() -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    
    func removeEmptySpaces() -> String
    {
        let outString = (self.trimmingCharacters(in: .whitespaces)) as String
        return outString
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}


extension UIViewController{
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    var navigationHeight:CGFloat {
        return self.hasTopNotch ? 124 : 100
    }

}

extension UIColor {
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue:
            CGFloat(blue) / 255.0, alpha: 1.0)
    }
        
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    
    // Blue
    static let loopDarkBlueColor = UIColor(red: 50.0 / 255.0, green: 49.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0) // #32315E
    
    static let loopBlueColor = UIColor(red: 0.0 / 255.0, green: 144.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0) // #0090FA
    
    // Gray
    static let loopDarkGrayColor = UIColor(red: 135.0 / 255.0, green: 152.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0) // #8798AB
    
    static let loopGrayColor = UIColor(red: 170.0 / 255.0, green: 183.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)          // #AAB7C5
    
    static let loopLightGrayColor = UIColor(red: 233.0 / 255.0, green: 238.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)     // #E9EEF5
    
    static let loopVeryLightGrayColor = UIColor(red: 246.0 / 255.0, green: 249.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0) // #F6F9FC
    
    // Green
    static let loopGreenColor = UIColor(red: 19.0 / 255.0, green: 181.0 / 255.0, blue: 125.0 / 255.0, alpha: 1.0)          // #13B57D
    
    static let loopLightGreenColor = UIColor(red: 173.0 / 255.0, green: 242.0 / 255.0, blue: 180.0 / 255.0, alpha: 1.0)    // #ADF2B4
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

public extension UIDevice {
    
    
    static let modelNumber: String = {
        
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return identifier
        
    }()
    
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
            // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}


private var kAssociationKeyMaxLength: Int = 0

extension UITextField {

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }

    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}

extension NSDictionary
{
    func stringWithBlank(forKey aKey: Any) -> String
    {
        if let str = self.value(forKeyPath: aKey as! String)
        {
            if str is NSNull
            {
                return ""
            }
            else if let s = str as? String
            {
                if s == "<null>"
                {
                    return ""
                }
            }
            return "\(str)"
        }
        return ""
    }
    
    func stringWithZero(forKey aKey: Any) -> String
    {
        if let str = self.value(forKeyPath: aKey as! String)
        {
            if str is NSNull
            {
                return "0"
            }
            else if let s = str as? String
            {
                if s == "<null>"
                {
                    return "0"
                }
            }
            return "\(str)"
        }
        return "0"
    }
}


extension UIViewController{
        func setStatusBarBackgroundColor(color:UIColor = .black){
               if #available(iOS 13.0, *) {
                   let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                   statusBar.isHidden = false
                   statusBar.backgroundColor = color
                   UIApplication.shared.keyWindow?.addSubview(statusBar)
               } else {
    //               UIApplication.shared.statusBarView?.backgroundColor = color
    //               UIApplication.shared.statusBarView?.isHidden = false
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = color
               }
           }
    
    func setNavigationBarClear(vc: UIViewController){
        if #available(iOS 13.0, *) {
            let navigationBar = vc.navigationController?.navigationBar
//           // let appearance = UINavigationBarAppearance()
//            let appearance = vc.navigationController?.navigationBar.compactAppearance
//            appearance?.setBackIndicatorImage(UIImage(), transitionMaskImage: UIImage())
//            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            navigationBar!.scrollEdgeAppearance = appearance
            navigationBar!.compactAppearance = appearance
            navigationBar!.standardAppearance = appearance
        }else{
            vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
              //     UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
            //        // Sets shadow (line below the bar) to a blank image
                    vc.navigationController?.navigationBar.shadowImage = UIImage()
            //        // Sets the translucent background color
                    vc.navigationController?.navigationBar.backgroundColor = .clear
            //        // Set translucent. (Default value is already true, so this can be removed if desired.)
                vc.navigationController?.navigationBar.isTranslucent = true
        }
        
    }
}
