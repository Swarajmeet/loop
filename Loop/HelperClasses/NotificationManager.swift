//
//  NotificationManager.swift
//  Loop
//
//  Created by KHOSA on 23/08/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationManager: NSObject {
    
   
    static let shared = NotificationManager()

    
    func getPendingNotifications(completion:@escaping (String) -> ())  {
        
        UNUserNotificationCenter.current().getPendingNotificationRequests {
            (requests) in
            
            for request in requests{
                completion(request.identifier)
            }
        }
        
    }
    
    
    func checkUserNotifications()  {
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        if let notifications = UserDataManager.notifications() {
            
            print(notifications)
            
            let daysArray = notifications.components(separatedBy: ",")
            
            for day in daysArray {
                self.scheduleLocalNotification(identifier: day)
            }
        }
    }
    
    
    func scheduleLocalNotification(identifier:String)  {
        self.scheduleLocalNotificationOnSelcetedDay(identifier: identifier)
    }
    
    
    func removescheduleLocalNotification(identifier:String) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    func checkAndCreateNewNotification(sender:UISwitch,identifier:String)  {
        
        if sender.isOn{
            self.checkNotificationStatus(identifier: identifier) { (status) in
                print(status)
                if !status{
                    self.scheduleLocalNotification(identifier: identifier)
                }
            }
            //            self.selectedDaysArray.add(identifier)
        }
        else{
            self.removescheduleLocalNotification(identifier: identifier)
            //            self.selectedDaysArray.remove(identifier)
        }
        
    }
    
    
    func checkNotificationStatus(identifier:String,completion:@escaping (Bool) -> ()) {
        
        UNUserNotificationCenter.current().getPendingNotificationRequests {
            (requests) in
            
            var isNotifiation = false
            
            for request in requests{
                
                if request.identifier == identifier{
                    isNotifiation = true
                }
            }
            completion(isNotifiation)
        }
    }



    
    func scheduleLocalNotificationOnSelcetedDay(identifier:String) {
        
        
        let dayDic = ["sunday":1,
                      "monday":2,
                      "tuesday":3,
                      "wednesday":4,
                      "thursday":5,
                      "friday":6,
                      "saturday":7]
        
        
        let dayNumber = dayDic[identifier]
        
        let content = UNMutableNotificationContent()
        content.title = "回收通知"
        content.body = "温馨提示：請把回收桶掛於門外，以便明天上午九時至下午六時回收。回收時段內沒有把回收桶掛於門外，本星期將不會回收"
        content.categoryIdentifier = identifier + ".category"
        content.sound = .default()
        
        var dateComponents = DateComponents()
        dateComponents.hour = 21
        dateComponents.minute = 00
        dateComponents.weekday = dayNumber
        dateComponents.second = 0
        
        let trigger = UNCalendarNotificationTrigger(
            dateMatching: dateComponents,
            repeats: true)
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("error :\(error.localizedDescription) in added notification  \(request.identifier)")
            }else{
                print("added notification:\(request.identifier)")
            }
        }
    }

}
