//
//  UserDataManager.swift
//  LookSee
//
//  Created by KHOSA on 10/06/19.
//  Copyright © 2019 WorksDelight. All rights reserved.
//

import Foundation

class UserDataManager: NSObject {
    
    class func saveData(userDic:[String:AnyObject]){
        UserDefaults.standard.set(userDic, forKey: "userData")
        UserDefaults.standard.synchronize()
    }
    
    class func userData() -> [String:AnyObject]?{
       
        let userDic = UserDefaults.standard.value(forKey: "userData") as? [String:AnyObject]
       
        print(userDic as Any)
        
        return userDic
    }
    
    
    
    class func userId() -> Int?{
        guard let userDic = self.userData() else {return nil}
        guard let userId = userDic["id"] as? Int else {return nil}
        return userId
    }
    
//    customer_id
    
    
    class func customer_Id() -> String?{
        guard let userDic = self.userData() else {return nil}
        guard let customer_Id = userDic["customer_id"] as? String else {return nil}
        return customer_Id
    }
    
   
    class func notifications() -> String? {
        guard let userDic = self.userData() else {return nil}
        guard let notifications = userDic["notifications"] as? String else {return nil}
        return notifications
    }

    
    class func isSubscribed() -> Bool{
        guard let userDic = self.userData() else {return false}
        guard let subscription_id = userDic["subscription_id"] as? String else {return false}
        return subscription_id.removeEmptySpaces() == "" ? false : true
    }

    
    
//    ["name": , "email": , "id": 3, "model_number": Simulator iPhone XR, "phone_number": 9041366321, "country_code": 91, "account_id": 2307089406210838, "os_version": 12.2, "device_token": , "device_manufacture": Apple, "created_at": 1562833065, "os_name": iOS, "device_model": Simulator iPhone XR, "country_name": 91]

    
    class func phoneNumber() -> String? {
      
        guard let userDic = self.userData() else {return nil}
        guard let country_code = userDic["country_code"] as? String else { return nil }
        guard let phone_number = userDic["phone_number"] as? String else { return nil }
        return country_code + phone_number
    }
    
    class func clear(){
        UserDefaults.standard.removeObject(forKey: "userData")
        UserDefaults.standard.synchronize()
    }
    
}
