//
//  Constants.swift
//

import Foundation
import UIKit

// MARK :: Street data
struct StreetData:Decodable {
    let status:Int
    let data:[StreetList]
    let message:String
}

struct StreetList:Decodable {
    var id:Int?
    var name:String?
}

// MARK: Order List

struct UserOrdersData:Decodable{
    let status:Int
    let data:[OrderList]
    let message:String
}

struct OrderList:Decodable {
    var id:Int?
    var user_id:String?
    var recycle_bin_id:String?
    var user_name:String?
    var phone_number:String?
    var street_number:String?
    var street_name:String?
    var building_name:String?
    var floor_number:String?
    var flat_number:String?
    var order_number:String?
    var created_at:String?
    var is_notification:String?
    var subscription_id:String?
    var is_cancelled:Int = 0
    var is_expired:Int = 0
}

let os_Name = UIDevice.current.systemName
let os_Version = UIDevice.current.systemVersion
let device_Manufacture  = "Apple"
let device_Model = UIDevice.modelName
let model_No = UIDevice.modelName
var device_token = ""


let APP_DELEGATE_OBJ = UIApplication.shared.delegate as! AppDelegate
//let BASE_URL = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com:3000/api/"

let BASE_URL = "http://theloopshk.com:3000/api/"


//http://theloopshk.com:3000


let SIGNUP_USER_URL = BASE_URL + "login"
let LOGOUT_URL = BASE_URL + "logout"
let GET_STREET_NAME_URL = BASE_URL + "get_streets"
let PLACE_ORDER_URL = BASE_URL + "place_order"
let GET_ORDERS = BASE_URL + "get_user_orders"
let SUBSCRIPTION_URL = BASE_URL + "payment"
let CANCEL_SUBSCRIPTION_URL = BASE_URL + "cancel_subscription"
let CREATE_EPHEMERAL_KEY_URL = BASE_URL + "create_ephemeral_key"
let GET_USER_PROFILE_URL = BASE_URL + "get_user_profile"
let GET_USER_SUBSCRIPTIONS = BASE_URL + "get_user_subscriptions"
let UPDATE_NOTIFICATIONS = BASE_URL + "update_notification_status"



