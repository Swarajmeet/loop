//
//  Animation.swift
//  ojaCustomer
//
//  Created by apple on 21/12/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit
class Animation {
    
    //For email validation
    static func shakeLabel(lbl:UILabel) {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.06
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: lbl.center.x - 10, y: lbl.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: lbl.center.x + 10, y: lbl.center.y))
        
        lbl.layer.add(animation, forKey: "position")
    }
}

