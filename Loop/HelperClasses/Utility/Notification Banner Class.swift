//
//  Notification Banner Alerts.swift
//  Notification Banner
//
//  Created by Swarajmeet Singh on 23/11/19.
//  Copyright © 2019 MeetiOSDeveloper. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift


class NotificationBannerAlerts: NSObject {
    
    static let shared = NotificationBannerAlerts()
    
    //MARK:- Error Alert
    let errorleftView = UIImageView(image: #imageLiteral(resourceName: "danger"))
    var errorMsg =  ""
    lazy var errorBanner = NotificationBanner(title: "錯誤", subtitle: errorMsg, leftView: errorleftView, style: .danger)
    
    
    let successleftView = UIImageView(image: #imageLiteral(resourceName: "success"))
    var successMsg = ""
    lazy var successBanner = NotificationBanner(title: "成功", subtitle: successMsg, leftView: successleftView, style: .success)
    
    
    //private override init() { }
    
     func showError(errormsg: String){
        errorMsg = errormsg
        errorBanner.duration = 0.7
        errorBanner.animationDuration = 0.4
       // errorBanner.haptic = .light
//        if let sublab = errorBanner.subtitleLabel{
//            sublab.text = errormsg
//        }
        errorBanner.subtitleLabel?.text = errormsg
        errorBanner.show(queuePosition: .back, bannerPosition: .top)
    }
    
    func showSuccess(successmsg: String){
//           successMsg = ""
//           successMsg = successmsg
           successBanner.duration = 0.8
           successBanner.animationDuration = 0.4
           successBanner.subtitleLabel?.text = successmsg
           successBanner.show(queuePosition: .back, bannerPosition: .top)
       }
    
    
    
    
    

}



