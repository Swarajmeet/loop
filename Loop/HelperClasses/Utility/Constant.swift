//
//  Constant.swift
//  Cuzinaa Business
//
//  Created by Worksdelight on 05/06/19.
//  Copyright © 2019 Worksdelight. All rights reserved.
//

import Foundation
import UIKit

var currentIndexItemId = String()
var currentCenterItemId = String()

class Constants {
    
    static let daysArr = ["sun","mon","tue","wed","thu","fri","sat"]
    
    static let daysFullArr = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    static let TABBAR_UNSELECTED_COLOR = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8477632705)
    
    static let TABBAR_SELECTED_COLOR = #colorLiteral(red: 1, green: 0.9058823529, blue: 0, alpha: 0.851837588)
    
    static let bookingStatusArr = ["Pending","Upcoming","Ongoing","History"]
    
    static let orderStatusArr = ["Pending","Ongoing","History"]
    
    static let kitchenMenuArr = ["Breakfast","Lunch","Dinner"]
    
    static let errorMessage = "Something went wrong,Please try again"
    
    static let internetError = "The internet connection appears to be offline"
    
    static let client_id = "5e2b8064-f4af-4cb8-9f8e-ef8e2a398fec"
    
    static let itemPortionOptions = ["PORTION SIZE","TOPPINGS"]
    
    static let itemPortionSize = ["Regular","Medium","Large"]
    
    static let zenDeskAccountKey = "ar9rXLq0R8lyYb6Wr75JoFCUGlE5JRnq"
    
    static let brainTreeClientToken = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpGVXpJMU5pSXNJbXRwWkNJNklqSXdNVGd3TkRJMk1UWXRjMkZ1WkdKdmVDSjkuZXlKbGVIQWlPakUxTmpVeE5UZzVPVFlzSW1wMGFTSTZJakl3TVRaaU5HWTBMVGhrTURjdE5EWmhNQzFoTm1KbExUYzJaak13TldJd01HVTJOeUlzSW5OMVlpSTZJak0wT0hCck9XTm5aak5pWjNsM01tSWlMQ0pwYzNNaU9pSkJkWFJvZVNJc0ltMWxjbU5vWVc1MElqcDdJbkIxWW14cFkxOXBaQ0k2SWpNME9IQnJPV05uWmpOaVozbDNNbUlpTENKMlpYSnBabmxmWTJGeVpGOWllVjlrWldaaGRXeDBJanBtWVd4elpYMHNJbkpwWjJoMGN5STZXeUp0WVc1aFoyVmZkbUYxYkhRaVhTd2liM0IwYVc5dWN5STZlMzE5Lmd5bl9SUmFCOEF6Qzl6aGhwWEZkZG5pUkFXdDhWUmZmdnJIdU9GamVscjN0VGkwalhSTnhOR1l5ZkUyTmN1V2pBMWRCY2FmcC1TVVBrWEpmMUF0Q2RBIiwiY29uZmlnVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaS92MS9jb25maWd1cmF0aW9uIiwiZ3JhcGhRTCI6eyJ1cmwiOiJodHRwczovL3BheW1lbnRzLnNhbmRib3guYnJhaW50cmVlLWFwaS5jb20vZ3JhcGhxbCIsImRhdGUiOiIyMDE4LTA1LTA4In0sImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9vcmlnaW4tYW5hbHl0aWNzLXNhbmQuc2FuZGJveC5icmFpbnRyZWUtYXBpLmNvbS8zNDhwazljZ2YzYmd5dzJiIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOnRydWUsInBheXBhbEVuYWJsZWQiOnRydWUsInBheXBhbCI6eyJkaXNwbGF5TmFtZSI6IkFjbWUgV2lkZ2V0cywgTHRkLiAoU2FuZGJveCkiLCJjbGllbnRJZCI6bnVsbCwicHJpdmFjeVVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS9wcCIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwOi8vZXhhbXBsZS5jb20vdG9zIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6dHJ1ZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6dHJ1ZSwibWVyY2hhbnRBY2NvdW50SWQiOiJhY21ld2lkZ2V0c2x0ZHNhbmRib3giLCJjdXJyZW5jeUlzb0NvZGUiOiJVU0QifSwibWVyY2hhbnRJZCI6IjM0OHBrOWNnZjNiZ3l3MmIiLCJ2ZW5tbyI6Im9mZiJ9"
}
