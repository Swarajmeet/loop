//
//  GlobalFunctions.swift
//  Cuzinaa Business
//
//  Created by Worksdelight on 05/06/19.
//  Copyright © 2019 Worksdelight. All rights reserved.
//

import Foundation
import UIKit

class Global{
    
    // email validation
    static func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    static func getAuthToken() -> String
    {
        var authToken = String()
        if UserDefaults.standard.value(forKey: "accessToken") != nil{
            authToken = UserDefaults.standard.value(forKey: "accessToken") as! String
            
        }
        return authToken
    }
    
    static func getImageUrl(url: String) -> String
    {
        var temp : [String] = []
        if  url != ""{
            temp = url.components(separatedBy: "/")
            let old_id = temp[temp.count - 1]
            let digits = old_id.replacingOccurrences(of: "\\D+", with: "", options: .regularExpression, range: nil)
            let myString: String = digits
            let lowerBound = String.Index.init(encodedOffset: 0)
            let upperBound = String.Index.init(encodedOffset: 2)
            let mySubstring: Substring = myString[lowerBound..<upperBound]
            
            if let id = Int(mySubstring) {
                let imageId = Int(id)%16
                let fakeurl = "http://d3lm6rfw7ydvrv.cloudfront.net/\(imageId).jpg";
                return fakeurl
            }
            else{
                return ""
            }
        }
        else{
            return ""
        }
        
    }
    
    static func getRefreshToken() -> String
    {
        var refreshToken = String()
        if UserDefaults.standard.value(forKey: "refreshToken") != nil{
            refreshToken = UserDefaults.standard.value(forKey: "refreshToken") as! String
            
        }
        return refreshToken
    }
    
    static func getUserSavedLocation() -> String
    {
        var loc : String = "-"
        if UserDefaults.standard.value(forKey: "userLocation") != nil{
            loc = UserDefaults.standard.value(forKey: "userLocation") as! String
            
        }
        return loc
    }
    
    static func getUserEmail() -> String
    {
        var email : String = "-"
        if UserDefaults.standard.value(forKey: "email") != nil{
            email = UserDefaults.standard.value(forKey: "email") as! String
            
        }
        return email
    }
    
    static func getuserName() -> String
    {
        var name : String = "-"
        if UserDefaults.standard.value(forKey: "name") != nil{
            name = UserDefaults.standard.value(forKey: "name") as! String
            
        }
        return name
    }
    
    static func getPhone() -> String
    {
        var msisdn : String = "-"
        if UserDefaults.standard.value(forKey: "msisdn") != nil{
            msisdn = UserDefaults.standard.value(forKey: "msisdn") as! String
            
        }
        return msisdn
    }
    
    static func getProfileId() -> String
    {
        var profile_id : String = "-"
        if UserDefaults.standard.value(forKey: "profile_id") != nil{
            profile_id = UserDefaults.standard.value(forKey: "profile_id") as! String
            
        }
        return profile_id
    }
    
    static func getConsumerId() -> String
    {
        var consumer_id : String = "-"
        if UserDefaults.standard.value(forKey: "consumer_id") != nil{
            consumer_id = UserDefaults.standard.value(forKey: "consumer_id") as! String
            
        }
        return consumer_id
    }
    
    static func getUuid() -> String
    {
        var uuid = String()
        if UserDefaults.standard.value(forKey: "UUID") != nil{
            uuid = UserDefaults.standard.value(forKey: "UUID") as! String
            
        }
        return uuid
    }
    
    static func getLatitude() -> String
    {
        var latitude = String()
        if UserDefaults.standard.value(forKey: "latitude") != nil{
            latitude = UserDefaults.standard.value(forKey: "latitude") as! String
            
        }
        return latitude
    }
    
    static func getLongitude() -> String
    {
        var longitude = String()
        if UserDefaults.standard.value(forKey: "longitude") != nil{
            longitude = UserDefaults.standard.value(forKey: "longitude") as! String
            
        }
        return longitude
    }
    
    static func getItemFavouriteListId() -> String
    {
        var itemFavouriteListId = String()
        if UserDefaults.standard.value(forKey: "itemFavouriteListId") != nil{
            itemFavouriteListId = UserDefaults.standard.value(forKey: "itemFavouriteListId") as! String
            
        }
        return itemFavouriteListId
    }
    
    static func getTrackOrderId() -> String
    {
        var trackOrderId = String()
        if UserDefaults.standard.value(forKey: "trackOrderId") != nil{
            trackOrderId = UserDefaults.standard.value(forKey: "trackOrderId") as! String
            
        }
        return trackOrderId
    }
    
    static func getTimeOfPlacaement() -> String
    {
        var timeOfPlacaement = String()
        if UserDefaults.standard.value(forKey: "timeOfPlacement") != nil{
            timeOfPlacaement = UserDefaults.standard.value(forKey: "timeOfPlacement") as! String
            
        }
        return timeOfPlacaement
    }
    
    static func getKitchenFavouriteListId() -> String
    {
        var kitchenFavouriteListId = String()
        if UserDefaults.standard.value(forKey: "kitchenFavouriteListId") != nil{
            kitchenFavouriteListId = UserDefaults.standard.value(forKey: "kitchenFavouriteListId") as! String
            
        }
        return kitchenFavouriteListId
    }
    
//    static func getkitchenFavouriteIdArr() -> [String]
//    {
//        var kitchenFavouriteIdArr = [String]()
//        if UserDefaults.standard.value(forKey: "kitchenFavouriteIdArr") != nil{
//            kitchenFavouriteIdArr = UserDefaults.standard.value(forKey: "kitchenFavouriteIdArr") as! [String]
//            Favourite.shared.kitchenFavouriteIdArr = kitchenFavouriteIdArr
//        }
//        return kitchenFavouriteIdArr
//    }
//    
//    static func getItemFavouriteIdArr() -> [String]
//    {
//        var itemFavouriteIdArr = [String]()
//        if UserDefaults.standard.value(forKey: "itemFavouriteIdArr") != nil{
//            itemFavouriteIdArr = UserDefaults.standard.value(forKey: "itemFavouriteIdArr") as! [String]
//            Favourite.shared.itemFavouriteIdArr = itemFavouriteIdArr
//        }
//        return itemFavouriteIdArr
//    }
    
    static func getMyKitchenId() -> String
    {
        var myKitchenId = String()
        if UserDefaults.standard.value(forKey: "myKitchen_id") != nil{
            myKitchenId = UserDefaults.standard.value(forKey: "myKitchen_id") as! String
            
        }
        return myKitchenId
    }
    
    static func getCartAddress() -> String
    {
        var cartAddress = String()
        if UserDefaults.standard.value(forKey: "CartAddress") != nil{
            cartAddress = UserDefaults.standard.value(forKey: "CartAddress") as! String
            
        }
        return cartAddress
    }
    
    static func getCartAddressId() -> String
    {
        var cartAddressId = String()
        if UserDefaults.standard.value(forKey: "CartAddressId") != nil{
            cartAddressId = UserDefaults.standard.value(forKey: "CartAddressId") as! String
            
        }
        return cartAddressId
    }
    
    static func getCartAddressCountryCode() -> String
    {
        var cartAddressCountryCode = String()
        if UserDefaults.standard.value(forKey: "CartAddressCountryCode") != nil{
            cartAddressCountryCode = UserDefaults.standard.value(forKey: "CartAddressCountryCode") as! String
            
        }
        return cartAddressCountryCode
    }
    
    static func getcartPaymentMode() -> String
    {
        var cartPaymentMode = String()
        if UserDefaults.standard.value(forKey: "cartPaymentMode") != nil{
            cartPaymentMode = UserDefaults.standard.value(forKey: "cartPaymentMode") as! String
            
        }
        return cartPaymentMode
    }
    
    static func getcartPaymentModeId() -> String
    {
        var cartPaymentModeId = String()
        if UserDefaults.standard.value(forKey: "cartPaymentModeId") != nil{
            cartPaymentModeId = UserDefaults.standard.value(forKey: "cartPaymentModeId") as! String
            
        }
        return cartPaymentModeId
    }
    
    static func getCartkitchenAddressId() -> String
    {
        var cartKitchenAddressId = String()
        if UserDefaults.standard.value(forKey: "cartKitchenAddressId") != nil{
            cartKitchenAddressId = UserDefaults.standard.value(forKey: "cartKitchenAddressId") as! String
            
        }
        return cartKitchenAddressId
    }

}
