//
//  PickImageClass.swift
//  OjaSeller
//
//  Created by apple on 06/12/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import Photos

class pickImage
{
    static func openGallery(vc:UIViewController,isVideo:Bool)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imag = UIImagePickerController()
            imag.delegate = vc as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imag.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imag.allowsEditing = true
            imag.mediaTypes =  isVideo ? ["public.image", "public.movie"] : ["public.image"]
            vc.present(imag, animated: true, completion: nil)
        }
    }
    
    static func openCamera(vc:UIViewController,isVideo:Bool)
    {
        let imag = UIImagePickerController()
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imag.delegate = vc as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imag.sourceType = UIImagePickerController.SourceType.camera
            imag.allowsEditing = true
            imag.mediaTypes =  isVideo ? [kUTTypeMovie as String,kUTTypeImage as String] : [kUTTypeImage as String]
            vc.present(imag, animated: true, completion: nil)
        }
        else
        {
            //UtilityClass.shared.showTSMessage(message: "You don't have camera", type: "warning")
        }
    }
    
     static func showActionSheet(vc:UIViewController,isVideo:Bool)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            openCamera(vc: vc, isVideo: isVideo)
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            openGallery(vc: vc, isVideo: isVideo)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true, completion: nil)
     }
    
    static func checkPhotoLibraryPermission() -> String {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            return "authorized"
        case .denied, .restricted :
            return "denied"
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    print("authorize")
                case .denied, .restricted:
                    print("denied")
                case .notDetermined:
                    print("not determined")
                }
            }
            return "notdetermined"
        }
    }
    
    static func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetLowQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    static func encodeVideo(videoURL: URL) -> URL?{
        let avAsset = AVURLAsset(url: videoURL)
        let startDate = Date()
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocPath = NSURL(fileURLWithPath: docDir).appendingPathComponent("temp.mp4")?.absoluteString
        
        let docDir2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = docDir2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath!)
        
        if FileManager.default.fileExists(atPath: myDocPath!){
            do{
                try FileManager.default.removeItem(atPath: myDocPath!)
            }catch let error{
                print(error)
            }
        }
        
        exportSession?.outputURL = filePath
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.shouldOptimizeForNetworkUse = true
        
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRange(start: start, duration: avAsset.duration)
        exportSession?.timeRange = range
        
        exportSession!.exportAsynchronously{() -> Void in
            switch exportSession!.status{
            case .failed:
                print("\(exportSession!.error!)")
            case .cancelled:
                print("Export cancelled")
            case .completed:
                let endDate = Date()
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful")
                print(exportSession?.outputURL ?? "")
            default:
                break
            }
        }
        return (exportSession?.outputURL)!
    }
    
    static func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else{
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
 }
