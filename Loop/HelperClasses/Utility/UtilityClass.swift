//
//  UtilityClass.swift
//  Cuzinaa Business
//
//  Created by Worksdelight on 03/06/19.
//  Copyright © 2019 Worksdelight. All rights reserved.
//

import Foundation
import UIKit
//import BRYXBanner
//import CleanyModal

class UtilityClass: NSObject {
    
    var didPressOk : (() -> Void)?
    
    static let shared : UtilityClass = {
        let instance = UtilityClass()
        return instance
    }()
    
    
    func detectDevice() -> Bool {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                return true
            case 1334:
                print("iPhone 6/6S/7/8")
                return true
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                return true
            case 2436:
                print("iPhone X, XS")
                return false
            case 2688:
                print("iPhone XS Max")
                return false
            case 1792:
                print("iPhone XR")
                return false
            default:
                print("Unknown")
                return false
            }
        }
        else{
            return false
        }
    }
    
    func getDays(day:String) -> String{
        if day == "sun" {
            return "Sunday"
        }
        else if day == "mon" {
            return "Monday"
        }
        else if day == "tue" {
            return "Tuesday"
        }
        else if day == "wed" {
            return "Wednesday"
        }
        else if day == "thu" {
            return "Thursday"
        }
        else if day == "fri" {
            return "Friday"
        }
        else{
            return "Saturday"
        }
    }
    
    func getDaysInNumber(day:String) -> Int{
        if day == "sun" {
            return 1
        }
        else if day == "mon" {
            return 2
        }
        else if day == "tue" {
            return 3
        }
        else if day == "wed" {
            return 4
        }
        else if day == "thu" {
            return 5
        }
        else if day == "fri" {
            return 6
        }
        else{
            return 7
        }
    }
    
    func selectDate(datePicker : UIDatePicker,type : String) -> String{
        let selectedDate = datePicker.date
        print(selectedDate)
        let formatter = DateFormatter()
        formatter.dateFormat = type == "date" ? "dd/MM/yyyy" : "h:mm a"
        let strDate = formatter.string(from: selectedDate)
        return strDate
    }
    
    func addLeftAlignedLabelOnNavigationTitle(viewC:UIViewController,titleTxt:String) {
        
        let width = UIScreen.main.bounds.width-100
        let titleView = UIView.init(frame: CGRect.init(x:0, y: 0, width: width, height: 40))
        let label = UILabel.init(frame: CGRect.init(x:0, y: 0, width: width, height: 36))
        label.textColor = UIColor.white
        label.text = titleTxt
        label.textAlignment = .left
        label.font = UIFont.init(name: "Futura-Medium", size: 17)
        label.minimumScaleFactor = 0.5
        titleView.addSubview(label)
        viewC.navigationItem.titleView = titleView
    }
    
    
//    func gotoCustomerHome() {
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//        let navigationController = UINavigationController(rootViewController: vc)
//        navigationController.navigationBar.isHidden = true
//        appDelegate?.window?.rootViewController = navigationController
//    }
    
//    func gotoCustomer() {
//
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//        let navigationController = UINavigationController(rootViewController: vc)
//        navigationController.navigationBar.isHidden = true
//        UIView.transition(with: appDelegate!.window!, duration: 0.9, options: UIView.AnimationOptions.transitionFlipFromLeft, animations: {
//            appDelegate?.window?.rootViewController = navigationController
//        }, completion: nil)
//    }
    
    func transparentNavigationBar(viewController:UIViewController,boolValue:Bool) {
        
        viewController.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        if boolValue{
            viewController.navigationController?.navigationBar.shadowImage = UIImage()
            viewController.navigationController?.navigationBar.isTranslucent = true
        }else{
            viewController.navigationController?.navigationBar.barTintColor = UIColor.init(red: 255.0/255.0, green: 231.0/255.0, blue: 0.0/255.0, alpha: 1)
            viewController.navigationController?.navigationBar.shadowImage = nil
            viewController.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    func flipViewController() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChefTabBarVC")
        UIView.transition(with: appDelegate!.window!, duration: 0.9, options: UIView.AnimationOptions.transitionFlipFromRight, animations: {
            let navigation = UINavigationController(rootViewController: vc)
            navigation.navigationBar.isHidden = true
            appDelegate!.window!.rootViewController = navigation
        }, completion: nil)
    }
    
//    func showTSMessage(message:String,type:String){
//
//        if type == "error" {
//            let banner = Banner(title: "Error!", subtitle: message, image: UIImage(named: "ErrorIcon"), backgroundColor: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))
//            banner.dismissesOnTap = true
//            banner.show(duration: 0.8)
//        }
//        else if type == "success" {
//            let banner = Banner(title: "Success", subtitle: message, image: UIImage(named: "SuccessIcon"), backgroundColor: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1))
//            banner.dismissesOnTap = true
//            banner.show(duration: 0.8)
//        }
//        else {
//            let banner = Banner(title: "Warning!", subtitle: message, image: UIImage(named: "WarningIcon"), backgroundColor: #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1))
//            banner.dismissesOnTap = true
//            banner.show(duration: 0.8)
//        }
//    }
//
//    func showAlert(title: String,message : String, vc : UIViewController) {
//
//        let alertConfig = CleanyAlertConfig(
//            title: title,
//            message: message)
//        let alert = CleanyAlert(config: alertConfig)
//
//        alert.addAction(title: "Yes", style: .cancel, handler: { (action) in
//            self.didPressOk!()
//            })
//        alert.addAction(title: "No", style: .cancel)
//
//        vc.present(alert, animated: true, completion: nil)
//    }
    
    func getCurrentDateTime(format: String) -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let result = formatter.string(from: date)
        return result
      
    }
    
    func getCurrentTimeZone() -> String{
        return TimeZone.current.identifier
    }
    
    func localToUTC(date:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mmZ"
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func localToUTC2(date:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "c"
        //dateFormatter.calendar = NSCalendar.current
       // dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
       // dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func UTCToLocal(date:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        //dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd.MM.yyyy, h:mm a"
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func UTCToLocal3(date:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        //dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd:MM.yyyy, h:mm a"
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func localToUTCTime(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "HH:mm"
        
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    
    func UTCToLocalTime(date:String,inFormat:String,outFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outFormat
        
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func localToUTCdate(date:String,inFormat:String,outFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = outFormat
        
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func calculatePrice(AddonPrice:String,type:String,currentPrice:String) -> String {
        let price = currentPrice
//        let p = price.components(separatedBy: " ")
        let priceInDouble : Double = NSString(string: price).doubleValue
        let AddonPrice : Double = NSString(string: AddonPrice).doubleValue
        let total = type == "add" ? priceInDouble + AddonPrice : priceInDouble - AddonPrice
        return "\(total)"
    }
    
    func getJsonFile(file:String) -> URL? {
        
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let fileUrl = documentDirectoryUrl.appendingPathComponent(file)
        return fileUrl
    }
    
    func removeSpace(text:String) -> String {
        let newString = text.replacingOccurrences(of: " ", with: "")
        return newString
    }
    
    func separateString(text:String,separator:String) -> [String] {
        let newString = text.components(separatedBy: separator)
        return newString
    }
    
    func expDateValidation(dateStr:String) -> Bool {
        
        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        
        let enterdYr = Int(dateStr.suffix(2)) ?? 0   // get last two digit from entered string as year
        let enterdMonth = Int(dateStr.prefix(2)) ?? 0  // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user
        
        if enterdYr > currentYear {
            
            if (1 ... 12).contains(enterdMonth){
                print("Entered Date Is Right")
                return true
            } else  {
                print("Entered Date Is Wrong")
                return false
            }
            
        } else  if currentYear == enterdYr {
            if enterdMonth >= currentMonth {
                if (1 ... 12).contains(enterdMonth) {
                    print("Entered Date Is Right")
                    return true
                }  else {
                    print("Entered Date Is Wrong")
                    return false
                    
                }
            } else {
                 print("Entered Date Is Wrong")
                 return false
               
            }
        } else {
             print("Entered Date Is Wrong")
             return false
        }
    }
    
    static func selectDate(datePicker : UIDatePicker,type : String) -> String
    {
        let selectedDate = datePicker.date
        print(selectedDate)
        let formatter = DateFormatter()
        formatter.dateFormat = type == "date" ? "dd/MM/yyyy" : "HH:mm"
        let strDate = formatter.string(from: selectedDate)
        return strDate
    }
    
}

    //MARK: delete OTP textfield
protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
        
}
//
//class CleanyAlert: CleanyAlertViewController {
//    
//    override init(config: CleanyAlertConfig) {
//        config.styleSettings[.tintColor] = UIColor(red: 8/255, green: 61/255, blue: 119/255, alpha: 1)
//        config.styleSettings[.destructiveColor] = UIColor.red
//        super.init(config: config)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}

class Days {
    
    var fullDays : [String] = []
    var days : [String] = []
   // var index : [Int] = []
    
    init(fullDays:[String], days: [String]) {
        self.fullDays = fullDays
        self.days = days
    }
    
}

