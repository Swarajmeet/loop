//
//  Device Details.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 25/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
import UIKit


class DeviceDetails: NSObject{
    static let shared = DeviceDetails()
    
    var os = ""
    var osVersion = ""
    var manufacture = ""
    var model = ""
    var deviceToken = ""
    var modelNumber = ""
    var countryName = ""
}


//class NotificationBannerAlerts: NSObject {
//
//static let shared = NotificationBannerAlerts()
