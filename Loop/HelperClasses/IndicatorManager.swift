//
//  IndicatorManager.swift
//  BreathTaking
//
//  Created by KHOSA on 06/04/18.
//  Copyright © 2018 BreathTaking. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import TSMessages


class IndicatorManager: NSObject,NVActivityIndicatorViewable {

    class func startIndicator(meesageStr:String? = nil) {
       
//        let color = UIColor.init(named: "0x8f8f8f")
        
        let color = UIColor.init(rgb: 0x8f8f8f)
        
        let size = CGSize(width: 60, height: 60)
        let activityData = ActivityData.init(size: size, message: meesageStr, type: .ballPulse, color: color, backgroundColor: .clear)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    class func stopIndicator() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
 
    class func showTSMessage(message:String,type:TSMessageNotificationType){
    
        let navController = APP_DELEGATE_OBJ.window?.rootViewController
        TSMessage.showNotification(in: navController, title: message, subtitle: "", type: type)
    }
    
}
