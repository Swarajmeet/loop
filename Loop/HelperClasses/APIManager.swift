//
//  APIManager.swift
//

import UIKit
import Alamofire
import SwiftyJSON

class APIManager: NSObject {
   
    static let shared : APIManager = {
        let instance = APIManager()
        return instance
    }()
    
    var headers = [
           "Content-Type": "application/x-www-form-urlencoded"
       ]
    
    func searchPostDataWebServiceMethod(_ parms: [String: AnyObject] , url: String ,completion:@escaping (NSDictionary,Bool) -> ()) {
        
        Alamofire.request(url, method: .post, parameters: parms, encoding: JSONEncoding.default).responseJSON
            { (response) in
                
                self.stopLoaders()
                
                let JSONResponse = response.result.value
                
                print(JSONResponse as Any)
                
                if response.response?.statusCode == 200
                {
                    if let responseDic = JSONResponse as? NSDictionary
                    {
                        let status = responseDic.value(forKey: "status") as! Bool
                        
                        if status{
                            completion(responseDic, true)
                        }else{
                            completion(NSDictionary.init(), false)
                            IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
                        }
                    }
                    else{
                        IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
                    }
                }
                else{
                    IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
                }
        }
    }
    
    
    func postDataToServer(_ parms: [String: AnyObject] , url: String, customHeader : [String:String] = APIManager.shared.headers ,completion:@escaping (NSDictionary,Bool) -> ()) {
    

//    let headers = [
//        "Content-Type": "application/x-www-form-urlencoded"
//    ]
  // headers = [String : String]()
    Alamofire.request(url, method: .post, parameters: parms, encoding:  URLEncoding.httpBody, headers: customHeader).responseJSON { (response) in
        print(url)
        print(parms)
        self.stopLoaders()
        print(response)
        let JSONResponse = response.result.value
        
        if response.response?.statusCode == 200
        {
            
            if let responseDic = JSONResponse as? NSDictionary
            {
                let status = responseDic.value(forKey: "status") as! Bool
                if status{
                    completion(responseDic, true)
                }else{
                    completion(responseDic, false)
//                    IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
                }
            }
            else{
                IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
            }
        }
        else{
            IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
        }
    }

    
//    Alamofire.request(url, method: .post, parameters: parms, encoding: JSONEncoding.default).responseJSON
//        { (response) in
//
//            self.stopLoaders()
//
//            let JSONResponse = response.result.value
//
//            print(JSONResponse as Any)
//
//            if response.response?.statusCode == 200
//            {
//                if let responseDic = JSONResponse as? NSDictionary
//                {
//                    let status = responseDic.value(forKey: "status") as! Bool
//
//                    if status{
//                        completion(responseDic, true)
//                    }else{
//                        IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
//                    }
//                }
//                else{
//                    IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
//                }
//            }
//            else{
//                IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
//            }
//    }
    }
    
    
    func searchUsersMultipartPostWebservice(_ url: String ,parms: NSDictionary,completion:@escaping (NSDictionary,Bool) -> ())
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parms
            {
                multipartFormData.append((value as! String).data(using:.utf8)! , withName: key as! String)
            }
            
        }, to: url) { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON (completionHandler: { (response) in
                    
                    self.stopLoaders()
                    let JSONResponse = response.result.value
                    
                    if response.response?.statusCode == 200
                    {
                        if let responseDic = JSONResponse as? NSDictionary
                        {
                            let status = responseDic.value(forKey: "status") as! Bool
                            
                            if status{
                                completion(responseDic, true)
                            }else{
                                completion(NSDictionary.init(), false)
                            }
                        }
                    }
                    else{
                        IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
                    }
                })
                
            case .failure(let encodingError):
                self.stopLoaders()
                print(encodingError.localizedDescription)
                IndicatorManager.showTSMessage(message: encodingError.localizedDescription, type: .error)
            }
        }
    }
    
    
    
    
    func multipartPostWebservice(_ url: String ,parms: NSDictionary,completion:@escaping (NSDictionary,Bool) -> ())
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parms
            {
                multipartFormData.append((value as! String).data(using:.utf8)! , withName: key as! String)
            }
            
        }, to: url) { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON (completionHandler: { (response) in
                    
                    
                    self.stopLoaders()

                    
                    let JSONResponse = response.result.value
                    
                    print(JSONResponse as Any)
                    
                    if response.response?.statusCode == 200
                    {
                        if let responseDic = JSONResponse as? NSDictionary
                        {
                            let status = responseDic.value(forKey: "status") as! Bool
                            
                            if status{
                                completion(responseDic, true)
                            }else{
                                IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
                                completion(responseDic, false)
                            }
                        }
                    }
                    else{
                       
                        completion(NSDictionary.init(), false)

                        if (response.error != nil)
                        {                           
                            IndicatorManager.showTSMessage(message: (response.error?.localizedDescription)!, type: .error)
                        }
                        else{
                            IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
                        }
                    }
                })
                
            case .failure(let encodingError):
                self.stopLoaders()
                completion(NSDictionary.init(), false)
                IndicatorManager.showTSMessage(message: encodingError.localizedDescription, type: .error)
            }
        }
    }
    
    
    
    
    func uploadImageMultipartPostWebService(_ url: String , ImageData:Data?,imageName:String? ,parms: NSDictionary,completion:@escaping (NSDictionary,Bool) -> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parms
            {
                multipartFormData.append((value as! String).data(using:.utf8 )! , withName: key as! String)
            }
            
            if ImageData != nil
            {
                multipartFormData.append(ImageData!, withName: imageName!, fileName:  imageName! + ".jpg", mimeType: "image/jpeg")
            }
            
        }, to: url) { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON (completionHandler: { (response) in
                    
                    self.stopLoaders()

                    let JSONResponse = response.result.value
                    
                    print(JSONResponse as Any)
                    
                    if response.response?.statusCode == 200
                    {
                        if let responseDic = JSONResponse as? NSDictionary
                        {
                            let status = responseDic.value(forKey: "status") as! Bool
                            
                            if status{
                                completion(responseDic, true)
                            }else{
                                IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
                            }
                        }
                    }
                    else{
                            IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
                    }
                })
            case .failure(let encodingError):
                self.stopLoaders()
                IndicatorManager.showTSMessage(message: encodingError.localizedDescription, type: .error)
            }
        }
    }
    
    
    func getDataFromServer_OneLineParsing(_ url: String,completion:@escaping (Data,Bool) -> ())
    {
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON { (response) in
            
            self.stopLoaders()
            
            let JSONResponse = response.result.value
            
            print(JSONResponse as Any)
            
            if response.response?.statusCode == 200
            {
                if let responseDic = JSONResponse as? NSDictionary
                {
                    print(responseDic)

                    let status = responseDic.value(forKey: "status") as! Bool
                    if status{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            
                            completion(response.data!, true)
                        })
                    }else{
                        IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
                    }
                }
            }
            else{
                IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
            }
        }
    }
    
    
  func getDataFromServer(_ url: String,completion:@escaping (NSDictionary,Bool) -> ())
  {
    
    
    Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON { (response) in

        self.stopLoaders()
        
        let JSONResponse = response.result.value
                
        print(url)

        print(JSONResponse as Any)

        if response.response?.statusCode == 200
        {
            if let responseDic = JSONResponse as? NSDictionary
            {
                let status = responseDic.value(forKey: "status") as! Bool
                if status{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        completion(responseDic, true)
                    })
                }else{
                    completion(NSDictionary.init(), false)
                    IndicatorManager.showTSMessage(message: responseDic.value(forKey: "message") as! String, type: .error)
                }
            }
        }
        else{
            completion(NSDictionary.init(), false)
            IndicatorManager.showTSMessage(message: "Something went wrong, Please try again later.", type: .error)
        }
    }
  }
    
    
    
    
    func stopLoaders() {
        IndicatorManager.stopIndicator()
    }

}

