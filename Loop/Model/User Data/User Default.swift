//
//  User Default.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 25/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import Foundation
struct UserDefaultKeys {
    static let id = "id"
    static let name = "name"
    static let email = "email"
    static let country_code = "country_code"
    static let phone_number = "phone_number"
    static let country_name = "country_name"
    static let os_name = "os_name"
    static let os_version = "os_version"
    static let device_manufacture = "device_manufacture"
    static let device_model = "device_model"
    static let device_token = "device_token"
    static let model_number = "model_number"
    static let account_id = "account_id"
    static let customer_id = "customer_id"
    static let created_at = "created_at"
    static let userDict = "userDict"
}
class UserDefaultData : NSObject{
    
    static let shared = UserDefaultData()
    
    
    
    func saveUserData(data: UserData){
        let dictToSave: NSDictionary = [UserDefaultKeys.id: data.id!,
                                        UserDefaultKeys.name: data.name!,
                                        UserDefaultKeys.email: data.email!,
                                        UserDefaultKeys.country_code: data.country_code!,
                                        UserDefaultKeys.phone_number: data.phone_number!,
                                        UserDefaultKeys.country_name: data.country_name!,
                                        UserDefaultKeys.os_name: data.os_name!,
                                        UserDefaultKeys.os_version: data.os_version!,
                                        UserDefaultKeys.device_manufacture: data.device_manufacture!,
                                        UserDefaultKeys.device_model: data.device_model!,
                                        UserDefaultKeys.device_token: data.device_token!,
                                        UserDefaultKeys.model_number: data.model_number!,
                                        UserDefaultKeys.account_id: data.account_id!,
                                        UserDefaultKeys.customer_id: data.customer_id!,
                                        UserDefaultKeys.created_at: data.created_at!,
        ]
        print(dictToSave)
        UserDefaults.standard.set(dictToSave, forKey: UserDefaultKeys.userDict)
        
    }
    
    func isUserLoggedIn() -> Bool{
        if let dict = UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as? NSDictionary {
            
            let id = dict.stringWithBlank(forKey: "id")
            
            if id.isEmpty
            {
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            return false
        }
    }
}
