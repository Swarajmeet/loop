

import Foundation
import UIKit



final class UserData: NSObject {
    
    static let shared = UserData()
    
    var id : String?
    var name : String?
    var email : String?
    var country_code : String?
    var phone_number : String?
    var country_name : String?
    var os_name : String?
    var os_version : String?
    var device_manufacture : String?
    var device_model : String?
    var device_token : String?
    var model_number : String?
    var account_id : String?
    var customer_id : String?
    var created_at : String?
    
    init(dict: NSDictionary! = nil) {
        super.init()
        
        if dict != nil
        {
            getUserData(dict: dict)
//            saveUserData(data: UserData(dict: dict))
        }
    }
    
    func getUserData(dict: NSDictionary){
        
        id = dict.stringWithBlank(forKey: "id")
        name = dict.stringWithBlank(forKey: "name")
        email = dict.stringWithBlank(forKey: "email")
        country_code = dict.stringWithBlank(forKey: "country_code")
        phone_number = dict.stringWithBlank(forKey: "phone_number")
        country_name = dict.stringWithBlank(forKey: "country_name")
        os_name = dict.stringWithBlank(forKey: "os_name")
        os_version = dict.stringWithBlank(forKey: "os_version")
        device_manufacture = dict.stringWithBlank(forKey: "device_manufacture")
        device_model = dict.stringWithBlank(forKey: "device_model")
        device_token = dict.stringWithBlank(forKey: "device_token")
        model_number = dict.stringWithBlank(forKey: "model_number")
        account_id = dict.stringWithBlank(forKey: "account_id")
        customer_id = dict.stringWithBlank(forKey: "customer_id")
        created_at = dict.stringWithBlank(forKey: "created_at")
    }
    
//    func saveUserData(data: UserData){
//        let dictToSave: NSDictionary = [UserDefaultKeys.id: data.id!,
//                                        UserDefaultKeys.name: data.name!,
//                                        UserDefaultKeys.email: data.email!,
//                                        UserDefaultKeys.country_code: data.country_code!,
//                                        UserDefaultKeys.phone_number: data.phone_number!,
//                                        UserDefaultKeys.country_name: data.country_name!,
//                                        UserDefaultKeys.os_name: data.os_name!,
//                                        UserDefaultKeys.os_version: data.os_version!,
//                                        UserDefaultKeys.device_manufacture: data.device_manufacture!,
//                                        UserDefaultKeys.device_model: data.device_model!,
//                                        UserDefaultKeys.device_token: data.device_token!,
//                                        UserDefaultKeys.model_number: data.model_number!,
//                                        UserDefaultKeys.account_id: data.account_id!,
//                                        UserDefaultKeys.customer_id: data.customer_id!,
//                                        UserDefaultKeys.created_at: data.created_at!,
//        ]
//        print(dictToSave)
//        UserDefaults.standard.set(dictToSave, forKey: UserDefaultKeys.userDict)
//        
//    }
    
}




//
//import Foundation
//import UIKit
//
//struct UserDefaultKeys {
//    static let id = "id"
//    static let name = "name"
//    static let email = "email"
//    static let country_code = "country_code"
//    static let phone_number = "phone_number"
//    static let country_name = "country_name"
//    static let os_name = "os_name"
//    static let os_version = "os_version"
//    static let device_manufacture = "device_manufacture"
//    static let device_model = "device_model"
//    static let device_token = "device_token"
//    static let model_number = "model_number"
//    static let account_id = "account_id"
//    static let customer_id = "customer_id"
//    static let created_at = "created_at"
//    static let userDict = "userDict"
//}
//final class UserData: NSObject {
//
//    static let shared = UserData()
//
//    var id : String?
//    var name : String?
//    var email : String?
//    var country_code : String?
//    var phone_number : String?
//    var country_name : String?
//    var os_name : String?
//    var os_version : String?
//    var device_manufacture : String?
//    var device_model : String?
//    var device_token : String?
//    var model_number : String?
//    var account_id : String?
//    var customer_id : String?
//    var created_at : String?
//
//    init(dict: NSDictionary! = nil) {
//        super.init()
//
//        if dict != nil
//        {
//            getUserData(dict: dict)
//            saveUserData(data: UserData(dict: dict))
//        }
//    }
//
//    func getUserData(dict: NSDictionary){
//
//        if let id = dict["id"] as? String{
//            self.id = String(describing: id)
//        }
//        if let name = dict["name"] as? String{
//            self.name = String(describing: name)
//        }
//        if let email = dict["email"] as? String{
//            self.email = String(describing: email)
//        }
//        if let country_code = dict["country_code"] as? String{
//            self.country_code = country_code
//        }
//        if let phone_number = dict["phone_number"]{
//            self.phone_number = String(describing: phone_number)
//        }
//        if let country_name = dict["country_name"]{
//            self.country_name = String(describing: country_name)
//        }
//        if let os_name = dict["os_name"]{
//            self.os_name = String(describing: os_name)
//        }
//        if let os_version = dict["os_version"]{
//            self.os_version = String(describing: os_version)
//        }
//        if let device_manufacture = dict["device_manufacture"]{
//            self.device_manufacture = String(describing: device_manufacture)
//        }
//        if let device_manufacture = dict["device_manufacture"]{
//            self.device_manufacture = String(describing: device_manufacture)
//        }
//        if let device_model = dict["device_model"]{
//            self.device_model = String(describing: device_model)
//        }
//        if let device_token = dict["device_token"]{
//            self.device_token = String(describing: device_token)
//        }
//        if let model_number = dict["model_number"]{
//            self.model_number = String(describing: model_number)
//        }
//        if let account_id = dict["account_id"]{
//            self.account_id = String(describing: account_id)
//        }
//        if let customer_id = dict["customer_id"]{
//            self.customer_id = String(describing: customer_id)
//        }
//        if let created_at = dict["created_at"]{
//            self.created_at = String(describing: created_at)
//        }
//
//    }
//
//    func saveUserData(data: UserData){
//        let dictToSave: NSDictionary = [UserDefaultKeys.id: data.id!,
//                                        UserDefaultKeys.name: data.name!,
//                                        UserDefaultKeys.email: data.email!,
//                                        UserDefaultKeys.country_code: data.country_code!,
//                                        UserDefaultKeys.phone_number: data.phone_number!,
//                                        UserDefaultKeys.country_name: data.country_name!,
//                                        UserDefaultKeys.os_name: data.os_name!,
//                                        UserDefaultKeys.os_version: data.os_version!,
//                                        UserDefaultKeys.device_manufacture: data.device_manufacture!,
//                                        UserDefaultKeys.device_model: data.device_model!,
//                                        UserDefaultKeys.device_token: data.device_token!,
//                                        UserDefaultKeys.model_number: data.model_number!,
//                                        UserDefaultKeys.account_id: data.account_id!,
//                                        UserDefaultKeys.customer_id: data.customer_id!,
//                                        UserDefaultKeys.created_at: data.created_at!,
//        ]
//        print(dictToSave)
//        UserDefaults.standard.set(dictToSave, forKey: UserDefaultKeys.userDict)
//
//    }
//
//}
//
//
//
