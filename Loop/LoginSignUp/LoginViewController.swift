//
//  LoginViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 22/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import TSMessages
class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButtton: UIButton!
    
    var tf = UITextField()
    
    var loginParams = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        setButtonTitle()
//        emailTextField.text = "swarajmeet.worksdelight@gmail.com"
//        passwordTextField.text = "123456"
//        print(DeviceDetails.shared.manufacture)
//        print(DeviceDetails.shared.os)
//        print(DeviceDetails.shared.osVersion)
//        print(DeviceDetails.shared.model)
//        print(DeviceDetails.shared.modelNumber)
//        print(DeviceDetails.shared.deviceToken)
//        print(DeviceDetails.shared.countryName)
        
    }
    
    // concatenate attributed strings
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBarClear(vc: self)
    }
    
    func loginValidation(){
        if let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if email != ""{
                let response = Validation.shared.validate(values: (ValidationType.email, email))
                switch response{
                case .success:
                    print(email)
                    loginParams.updateValue(email as AnyObject, forKey: "email")
                case .failure(_):
                    NotificationBannerAlerts.shared.showError(errormsg: "電子郵件格式無效") //Invalid email format
                    emailTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
                
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: "電子郵件不能為空") //Email cannot be empty
                emailTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
            
        }
        if let password = passwordTextField.text{
            if password != ""{
                loginParams.updateValue(password as AnyObject, forKey: "password")
                
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: "請輸入密碼") //Please enter your password
                passwordTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        loginParams.updateValue(DeviceDetails.shared.os  as AnyObject, forKey: "os_name")
        loginParams.updateValue(DeviceDetails.shared.osVersion as AnyObject, forKey: "os_version")
        loginParams.updateValue(DeviceDetails.shared.manufacture as AnyObject, forKey: "device_manufacture")
        loginParams.updateValue(DeviceDetails.shared.model as AnyObject, forKey: "device_model")
        loginParams.updateValue(DeviceDetails.shared.deviceToken as AnyObject, forKey: "device_token")
        loginParams.updateValue(DeviceDetails.shared.modelNumber as AnyObject, forKey: "model_number")
        loginParams.updateValue(DeviceDetails.shared.countryName as AnyObject, forKey: "country_name")
        
        print(loginParams)
        loginWebservice(params: loginParams)
        //goToHome()
    }
    
    func loginWebservice(params: [String:AnyObject]){
        let urlString = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com/loop/index.php/api/login"
        
        APIManager.shared.postDataToServer(params, url: urlString) { (response, success) in
            print(response)
            
            
            IndicatorManager.stopIndicator()
            if success{
                if let data = response.value(forKey: "data") as? NSDictionary{
                    let userData = UserData(dict: data)
                    UserDefaultData.shared.saveUserData(data: userData)
                    print(userData)

                }
                self.goToHome()
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: response["message"] as! String)
            }
        }
        
    }
    
    
    func goToHome(){
        IndicatorManager.stopIndicator()
        NotificationBannerAlerts.shared.showSuccess(successmsg: "Login Successful")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarVc = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = tabBarVc
        
    }
    
    
    
    
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        IndicatorManager.startIndicator()
        loginValidation()
        
    }
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EmailVerificationViewController") as! EmailVerificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setButtonTitle(){
        let buttonTitle = "新來的？ 註冊"
        let textBlack = "新來的？ "
        let textRed = "註冊"
        let fonts = UIFont.systemFont(ofSize: 18)
        
        let fullRange = (buttonTitle as NSString).range(of: buttonTitle)
        let blackRange = (buttonTitle as NSString).range(of: textBlack)
        let redRange = (buttonTitle as NSString).range(of: textRed)
        let attributedString = NSMutableAttributedString(string:buttonTitle)
        attributedString.addAttribute(NSAttributedString.Key.font, value:  fonts, range: fullRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) , range: blackRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.3154407442, green: 0.8673143983, blue: 0.7914745212, alpha: 1) , range: redRange)
        signUpButtton.setAttributedTitle(attributedString, for: .normal)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LoginViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.superview?.borderColor = .white
        return true
    }
}



