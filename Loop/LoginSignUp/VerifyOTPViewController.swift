//
//  VerifyOTPViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 25/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class VerifyOTPViewController: UIViewController {
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var otpTextField: UITextField!
    var email = ""
    var otpText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        emailLabel.text = email
        
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        IndicatorManager.startIndicator()
        checkOtp()
    }
    
    
    func checkOtp(){
        if let otp = otpTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if otp == ""{
                NotificationBannerAlerts.shared.showError(errormsg: "請輸入OTP") //OTP cannot be empty
                otpTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }else{
                if otpTextField.text == otpText{
                    IndicatorManager.stopIndicator()
                    NotificationBannerAlerts.shared.showSuccess(successmsg: "已驗證") //Verified
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                        let storyBoard = UIStoryboard(name: "LoginSignUp", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
                        vc.email = self.email
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }else{
                    IndicatorManager.stopIndicator()
                    NotificationBannerAlerts.shared.showError(errormsg: "OTP錯誤") // Wrong Otp
                }
            }
        }
    }
    
    
    
    
    
}

extension VerifyOTPViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.superview?.borderColor = .white
        return true
    }
}
