//
//  SignUpViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 22/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var email = ""
    var signUpParams = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        emailTextField.text = email
        // Do any additional setup after loading the view.
        
    }
    
    
    
    
    
    //    func gotoVerifyEmail(){
    //        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "EmailVerificationViewController") as! EmailVerificationViewController
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    
    func goToHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarVc = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = tabBarVc
        
    }
    
    func signUpValidation(){
        
        if let name = userNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if name != ""{
                let response = Validation.shared.validate(values: (ValidationType.alphabeticString, name))
                switch response{
                case .success:
                    print(name)
                    signUpParams.updateValue(name as AnyObject, forKey: "name")
                case .failure(_):
                    NotificationBannerAlerts.shared.showError(errormsg: "用戶名無效") //Username not vaild
                    userNameTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
            }else{
                
                NotificationBannerAlerts.shared.showError(errormsg: "用戶名不能為空") //Username can't be empty
                userNameTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        if let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if email != ""{
                let response = Validation.shared.validate(values: (ValidationType.email, email))
                switch response{
                case .success:
                    print(email)
                    signUpParams.updateValue(email as AnyObject, forKey: "email")
                case .failure(_):
                    
                    NotificationBannerAlerts.shared.showError(errormsg: "電子郵件格式無效") // Invalid Email
                    emailTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
            }else{
                
                NotificationBannerAlerts.shared.showError(errormsg: "電子郵件不能為空") //Email can't be empty
                emailTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        if let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if password != ""{
                if password.count >= 6{
                    print(password)
                }else{
                    
                    NotificationBannerAlerts.shared.showError(errormsg: "至少6位數字的密碼") //Password length minimum 6 digits
                    passwordTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                    
                }
            }else{
                
                NotificationBannerAlerts.shared.showError(errormsg: "請輸入密碼") //Password can't be empty
                passwordTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        if let confirmPassword = confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if confirmPassword != ""{
                if confirmPassword == passwordTextField.text{
                    print(confirmPassword)
                    signUpParams.updateValue(confirmPassword as AnyObject, forKey: "password")
                }else{
                    NotificationBannerAlerts.shared.showError(errormsg: "密碼不匹配") // Password doesn't match
                    confirmPasswordTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
            }else{
                
                NotificationBannerAlerts.shared.showError(errormsg: "確認密碼不能為空") //Confirm Password can't be empty
                confirmPasswordTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        signUpParams.updateValue(DeviceDetails.shared.os  as AnyObject, forKey: "os_name")
        signUpParams.updateValue(DeviceDetails.shared.osVersion as AnyObject, forKey: "os_version")
        signUpParams.updateValue(DeviceDetails.shared.manufacture as AnyObject, forKey: "device_manufacture")
        signUpParams.updateValue(DeviceDetails.shared.model as AnyObject, forKey: "device_model")
        signUpParams.updateValue(DeviceDetails.shared.deviceToken as AnyObject, forKey: "device_token")
        signUpParams.updateValue(DeviceDetails.shared.modelNumber as AnyObject, forKey: "model_number")
        signUpParams.updateValue(DeviceDetails.shared.countryName as AnyObject, forKey: "country_name")
        
        print(signUpParams)
        signUpWebservice(params: signUpParams)
        //gotoVerifyEmail()
        
    }
    
    func signUpWebservice(params: [String:AnyObject]){
        let urlString = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com/loop/index.php/api/register"
        
        APIManager.shared.postDataToServer(params, url: urlString) { (response, success) in
            print(response)
            IndicatorManager.stopIndicator()
            if success{
                if let data = response.value(forKey: "data") as? NSDictionary{
                    let userData = UserData(dict: data)
                    UserDefaultData.shared.saveUserData(data: userData)
                    print(userData)
                }
                    
                    NotificationBannerAlerts.shared.showSuccess(successmsg: response["message"] as! String)
                
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                        self.goToHome()
                    }
                }else{
                    NotificationBannerAlerts.shared.showError(errormsg: response["message"] as! String)
                }
            }
            
        }
        
        
        @IBAction func signUpButtonTapped(_ sender: UIButton) {
            IndicatorManager.startIndicator()
            signUpValidation()
        }
        
        //    @IBAction func popToLoginButtonTapped(_ sender: UIButton) {
        //        self.navigationController?.popViewController(animated: true)
        //    }
        
    }
    
    extension SignUpViewController: UITextFieldDelegate{
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            textField.superview?.borderColor = .white
            return true
        }
}
