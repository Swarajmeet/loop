//
//  NewPasswordViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 23/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class NewPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var otpTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    var email = String()
    var otp = ""
    var newPasswordToken = ""
    var newPasswordParmas = [String:AnyObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        emailLabel.text = email
        
    }
    func forgotPasswordVaildation(){
        
        if let otptext = otpTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if otptext == ""{
                NotificationBannerAlerts.shared.showError(errormsg: "請輸入OTP") //OTP cannot be empty
                otpTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
            
            if otptext != otp{
                NotificationBannerAlerts.shared.showError(errormsg: "OTP錯誤")
                otpTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        if let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if password != ""{
                if password.count >= 6{
                    print(password)
                }else{
                    NotificationBannerAlerts.shared.showError(errormsg: "至少6位數字的密碼") //Password length minimum 6 digits
                    passwordTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                    
                }
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: "請輸入密碼") //Password can't be empty
                passwordTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        if let confirmPassword = confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if confirmPassword != ""{
                if confirmPassword == passwordTextField.text{
                    newPasswordParmas.updateValue(confirmPassword as AnyObject, forKey: "password")
                }else{
                    NotificationBannerAlerts.shared.showError(errormsg: "密碼不匹配") // Password doesn't match
                    confirmPasswordTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
            }else{
                
                NotificationBannerAlerts.shared.showError(errormsg: "確認密碼不能為空") //Confirm Password can't be empty
                confirmPasswordTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
        }
        
        //gotoLoginScreen()
        newPasswordWebservice(params: newPasswordParmas)
    }
    
    
    func newPasswordWebservice(params: [String:AnyObject]){
        let urlString = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com/loop/index.php/api/changePassword"
        let headers = ["Content-Type": "application/x-www-form-urlencoded",
                       "token": newPasswordToken]
        
        APIManager.shared.postDataToServer(params, url: urlString, customHeader: headers) { (response, success) in
            print(response)
            IndicatorManager.stopIndicator()
            if success{
                
                NotificationBannerAlerts.shared.showSuccess(successmsg: response["message"] as! String)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                    self.gotoLoginScreen()
                }
            }else{
                if let error = response["message"]{
                    NotificationBannerAlerts.shared.showError(errormsg: error as! String)
                }
                
            }
        }
        
    }
    
    
    func gotoLoginScreen(){
        IndicatorManager.stopIndicator()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) { // Change `2.0` to the desired number of seconds.
            let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.backToViewController(vc: vc)
        }
    }
    
    @IBAction func savePasswordButtonTapped(_ sender: UIButton) {
        IndicatorManager.startIndicator()
        forgotPasswordVaildation()
    }
    
    
    
}

extension NewPasswordViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.superview?.borderColor = .white
        return true
    }
}

extension UINavigationController {
    
    func backToViewController(vc: UIViewController) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if type(of:element)  == type(of:vc)  {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}

//extension UINavigationController {
//
//   func backToViewController(vc: Any) {
//      // iterate to find the type of vc
//      for element in viewControllers as Array {
//         if "\(element.dynamicType).Type" == "\(vc.dynamicType)" {
//            self.popToViewController(element, animated: true)
//            break
//         }
//      }
//   }
//
//}
