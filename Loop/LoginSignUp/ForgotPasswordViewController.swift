//
//  ForgotPasswordViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 25/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    
    var forgotParams = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendOtpTapped(_ sender: UIButton) {
        IndicatorManager.startIndicator()
        forgotPasswordValidation()
    }
    @IBAction func backToSignInButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func forgotPasswordValidation(){
        
        if let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if email != ""{
                let response = Validation.shared.validate(values: (ValidationType.email, email))
                switch response{
                case .success:
                    
                    forgotParams.updateValue(email as AnyObject, forKey: "email")
                    
                case .failure(_):
                    NotificationBannerAlerts.shared.showError(errormsg: "電子郵件格式無效") //Invalid email format
                    emailTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
                
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: "電子郵件不能為空") //Email cannot be empty
                emailTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
            
        }
        
        fogotPasswordWebservice(params: forgotParams)
        
    }
    
    func fogotPasswordWebservice(params: [String:AnyObject]){
        let urlString = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com/loop/index.php/api/forget"
        
        APIManager.shared.postDataToServer(params, url: urlString) { (response, success) in
            print(response)
            if success{
                IndicatorManager.stopIndicator()
                NotificationBannerAlerts.shared.showSuccess(successmsg: response["message"] as! String)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                    let storyBoard = UIStoryboard(name: "LoginSignUp", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "NewPasswordViewController") as! NewPasswordViewController
                    vc.email = self.emailTextField.text!
                    vc.otp = response["otp"] as! String
                    vc.newPasswordToken = response["token"] as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                if let error = response["message"]{
                    IndicatorManager.stopIndicator()
                    NotificationBannerAlerts.shared.showError(errormsg: error as! String)
                }
                
            }
        }
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension ForgotPasswordViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.superview?.borderColor = .white
        return true
    }
}

