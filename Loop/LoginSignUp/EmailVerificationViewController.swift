//
//  EmailVerificationViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 22/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EmailVerificationViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var backLoginButton: UIButton!
    var verifyEmailParams = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        // Do any additional setup after loading the view.
        setButtonTitle()
    }
    
    func setButtonTitle(){
        let buttonTitle = "已經是會員？ 登錄"
        let textBlack = "已經是會員？"
        let textRed = "登錄"
        let fonts = UIFont.systemFont(ofSize: 18)
        
        let fullRange = (buttonTitle as NSString).range(of: buttonTitle)
        let blackRange = (buttonTitle as NSString).range(of: textBlack)
        let redRange = (buttonTitle as NSString).range(of: textRed)
        let attributedString = NSMutableAttributedString(string:buttonTitle)
        attributedString.addAttribute(NSAttributedString.Key.font, value:  fonts, range: fullRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) , range: blackRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.3154407442, green: 0.8673143983, blue: 0.7914745212, alpha: 1) , range: redRange)
        backLoginButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    
    
    
    func verifyEmailValidation(){
        
        if let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            if email != ""{
                let response = Validation.shared.validate(values: (ValidationType.email, email))
                switch response{
                case .success:
                    verifyEmailParams.updateValue(email as AnyObject, forKey: "email")
                case .failure(_):
                    NotificationBannerAlerts.shared.showError(errormsg: "電子郵件格式無效") //Invalid email format
                    emailTextField.superview?.borderColor = .red
                    IndicatorManager.stopIndicator()
                    return
                }
                
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: "電子郵件不能為空") //Email cannot be empty
                emailTextField.superview?.borderColor = .red
                IndicatorManager.stopIndicator()
                return
            }
            
        }
        
        verifyEmailWebservice(params: verifyEmailParams)
        
    }
    
    func verifyEmailWebservice(params: [String:AnyObject]){
        let urlString = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com/loop/index.php/api/emailVerify"
        print(params)
        APIManager.shared.postDataToServer(params, url: urlString) { (response, success) in
            print(response)
            IndicatorManager.stopIndicator()
            if success{
                NotificationBannerAlerts.shared.showSuccess(successmsg: response["message"] as! String)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
                    let storyBoard = UIStoryboard(name: "LoginSignUp", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
                    vc.email = self.emailTextField.text!
                    vc.otpText = response["otp"] as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }else{
                NotificationBannerAlerts.shared.showError(errormsg: response["message"] as! String)
            }
        }
        
    }
    
    
    @IBAction func sendOTPButtonTapped(_ sender: UIButton) {
        //checkOtp()
        IndicatorManager.startIndicator()
        verifyEmailValidation()
        
        
    }
    
    
    @IBAction func backToLoginButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}
extension EmailVerificationViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.superview?.borderColor = .white
        return true
    }
}
