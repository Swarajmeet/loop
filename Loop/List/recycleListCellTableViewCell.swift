//
//  recycleListCellTableViewCell.swift
//  Loop
//
//  Created by Jorch Wong on 2/4/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class recycleListCellTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productCategory: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
