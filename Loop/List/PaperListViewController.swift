//
//  PaperListViewController.swift
//  Loop
//
//  Created by WorksDelight Mac 6 on 26/11/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import SimpleImageViewer
class PaperListViewController: UIViewController {
    
    @IBOutlet weak var paperListImageView: UIImageView!
    
    var image = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paperListImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewImage(tapGestureRecognizer:))))
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func viewImage(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        let configuration = ImageViewerConfiguration { config in
            config.imageView = paperListImageView
        }
        
        present(ImageViewerController(configuration: configuration), animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

