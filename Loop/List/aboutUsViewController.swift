//
//  aboutUsViewController.swift
//  Loop
//
//  Created by Jorch Wong on 2/4/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import AccountKit
import UserNotifications

class aboutUsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
 
    
    
    @IBOutlet var tableView: UITableView!
    var items = [String]()
    var identities = ["A", "B"]

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       self.decideTableData()
    }
    
    func decideTableData()  {
        
//        if UserDataManager.userId() != nil{
////            items = ["關於我們","條款與細則","訂閱歷史記錄","登出"]
//            items = ["關於我們","條款與細則","設定提醒通知","聯絡我們","登出"]
//        }else{
//            items = ["關於我們","條款與細則","設定提醒通知","聯絡我們"]
//
//        }
//
        if let email = (UserDefaults.standard.value(forKey: UserDefaultKeys.userDict) as! NSDictionary)["email"]{
            let email = email as! String
            if email == ""{
                items = ["關於我們","條款與細則","設定提醒通知","聯絡我們"]
            }else{
                items = ["關於我們","條款與細則","設定提醒通知","聯絡我們","登出"]
            }
        }else{
            items = ["關於我們","條款與細則","設定提醒通知","聯絡我們"]
        }
        
        self.tableView.reloadData()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel!.text = items [indexPath.row]
        cell?.selectionStyle = .none
        tableView.tableFooterView = UIView(frame: .zero)
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        tableView.deselectRow(at: indexPath, animated: true)
        
        let titleStr = items [indexPath.row]
        
        if titleStr == "登出" {
            DispatchQueue.main.async {
                self.logoutWarning()
            }
        }else if titleStr == "聯絡我們"{
            self.openMessenger()
        }else if titleStr == "設定提醒通知"{
            let destinationVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(destinationVc, animated: true)
        }
        else{
            let vcName = identities [indexPath.row]
            let viewCotroller = storyboard?.instantiateViewController(withIdentifier: vcName)
            self.navigationController?.pushViewController(viewCotroller!, animated: true)
        }
        
//        else if titleStr == "訂閱歷史記錄"{
//            let destinationVc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionHistoryVc") as! SubscriptionHistoryVc
//            self.navigationController?.pushViewController(destinationVc, animated: true)
//        }
    }
    
    func openMessenger()  {
        
        let url = URL(string: "http://m.me/theLoopRecycle")
        
        UIApplication.shared.open(url!, options: [:], completionHandler: {
            (success) in
            
            if success == false {
                // Messenger is not installed. Open in browser instead.
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!)
                }
            }
        })
        

    }
    
    
    // MARK:: ALERT METHODS
    
    
    
    func signInUserAPI(params:[String:AnyObject])  {
        
        APIManager.shared.postDataToServer(params, url: SIGNUP_USER_URL) { (response, success) in
            
            IndicatorManager.stopIndicator()
            
            if success{
                 print(response)
                let userData = response.value(forKey: "data")
                UserDataManager.saveData(userDic: userData as! [String:AnyObject])
            }
        }
    }
    
    func logoutWarning() {
        
//        你確定要登出嗎？
        
//        "你确定要戒掉吗？"
        
        let alert = UIAlertController(title: "登出", message: "你確定要登出嗎？", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "否", style: .cancel, handler: nil)
        
        let deleteAction = UIAlertAction(title: "是", style: .destructive) { _ in
            DispatchQueue.main.async {
               // self.logoutAPI()
                self.logout()
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        present(alert, animated: true, completion: nil)
    }
    
    func logout(){
        //UserDefaults.standard.set(dictToSave, forKey: UserDefaultKeys.userDict)
        let emptyDict = [String:AnyObject]()
        UserDefaults.standard.set(emptyDict, forKey: UserDefaultKeys.userDict)
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let loginNavBar = storyboard.instantiateViewController(withIdentifier: "LoginNavBar") as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = loginNavBar
        UIView.transition(with:  appDelegate.window!,duration: 0.3,options: .transitionCrossDissolve,animations: nil,completion: nil)
    }
    
    
    
    func logoutAPI()  {
        
        guard let userid = UserDataManager.userId() else { return  }
        
        let params = ["user_id":userid]
        
        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(params as [String : AnyObject], url: LOGOUT_URL) { (response, success) in
            
            IndicatorManager.stopIndicator()
            
            if success{
                UserDataManager.clear()
                self.decideTableData()
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            }
        }
    }
    
    @IBAction func instagram(_ sender: UIButton) {
        openUrl(urlStr: "http://www.instagram.com")
    }
    
    @IBAction func facebook(_ sender: UIButton) {
        openUrl(urlStr: "http://www.facebook.com")
    }
    
    func openUrl(urlStr:String!) {
        
        if let url = NSURL(string:urlStr) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
