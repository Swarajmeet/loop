//
//  recycleListPageOneViewController.swift
//  Loop
//
//  Created by Jorch Wong on 4/4/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class recycleListPageOneViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        paperButton.layer.cornerRadius = 10.0
        paperButton.layer.masksToBounds = true
        plasticButton.layer.cornerRadius = 10.0
        plasticButton.layer.masksToBounds = true
        paperPackButton.layer.cornerRadius = 10.0
        paperPackButton.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barStyle = .default
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @IBOutlet weak var paperButton: UIButton!
    @IBOutlet weak var plasticButton: UIButton!
    @IBOutlet weak var paperPackButton: UIButton!
    
    @IBAction func gotoPaperList(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PaperListViewController") as! PaperListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}





/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


