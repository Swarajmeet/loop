//
//  SubscriptionHistoryVc.swift
//  Loop
//
//  Created by KHOSA on 22/07/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

//"start_date": 1563530802,
//"end_date": 1566209202,
//"status": "active",
//"amount": 999,
//"cancel_at_period_end": true,
//"payment_mode": "card"

struct SubscriptionHistoryModel:Decodable {
    var start_date:Double?
    var end_date:Double?
    var status:String?
    var currency:String?
    var amount:Int? = 0
    var cancel_at_period_end:Bool?
    var payment_mode:String?
}


class SubscriptionHistoryVc: UIViewController {

    @IBOutlet var tableView: UITableView!
    var subscriptionsArray = [SubscriptionHistoryModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getUserSubscriptions()
    }
    
    
    func getUserSubscriptions()  {
        
        guard let userId = UserDataManager.userId() else { return }
        
        IndicatorManager.startIndicator()
        
        APIManager.shared.postDataToServer(["user_id":userId as AnyObject], url: GET_USER_SUBSCRIPTIONS) { (result, success) in
            
            IndicatorManager.stopIndicator()
            
            if success{
                
                guard let result = result as? [String:AnyObject] else {return}
                let status = result["status"] as? Bool
                
                
                print(result)
                
                if status! {
                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject: result["data"] as Any, options: .prettyPrinted)
                        self.subscriptionsArray = try JSONDecoder().decode([SubscriptionHistoryModel].self, from: data)
                       
                        
                        self.tableView.reloadData()
                    }
                    catch let jsonErr {
                        print("Error serializing json: ", jsonErr)
                    }
                }
            }else{
            }

            }
        }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func unixDateToDate(timeResult:Double) -> String {
        let date = Date(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }

}



extension SubscriptionHistoryVc: UITableViewDelegate,UITableViewDataSource {
    
    // MARK:: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.subscriptionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionHistoryCell", for: indexPath) as? SubscriptionHistoryCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let dataModel = self.subscriptionsArray[indexPath.row]

//        let formatter = NumberFormatter()
//        formatter.locale = Locale.init(identifier: "hk")
//        formatter.numberStyle = .currency
//        if let formattedTipAmount = formatter.string(from: dataModel.amount! as NSNumber) {
//
//            print(formattedTipAmount)
//            cell.amountLabel.text = "Tip Amount: \(formattedTipAmount)"
//        }
        
        
        
        cell.startDateLabel.text = self.unixDateToDate(timeResult: dataModel.start_date!)
        cell.endDateLabel.text = self.unixDateToDate(timeResult: dataModel.end_date!)
        cell.statusLabel.text = dataModel.status
        cell.paymentModeLabel.text = dataModel.payment_mode
        cell.amountLabel.text = "HKD 9.99"
        return cell
        
    }
    
    
    
    // MARK:: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
