//
//  recycleListViewController.swift
//  Loop
//
//  Created by Jorch Wong on 2/4/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class recycleListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    @IBOutlet weak var recycleListTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
 
    var animalArray = [Animal]()
    var currentAnimalArray = [Animal]() //update table
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAnimals()
        setUpSearchBar()
        alterLayout()
    }
    
    private func setUpAnimals() {
        // CATS
        animalArray.append(Animal(productName: "麥提沙小包裝", productCategory: .recyclable))
        animalArray.append(Animal(productName: "灣仔碼頭水餃", productCategory: .recyclable))
        animalArray.append(Animal(productName: "維他奶盒", productCategory: .recyclable))
        // DOGS
        animalArray.append(Animal(productName: "橡膠", productCategory: .unrecyclable))
        animalArray.append(Animal(productName: "收據", productCategory: .unrecyclable))
        animalArray.append(Animal(productName: "膠紙", productCategory: .unrecyclable))
        
        currentAnimalArray = animalArray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        IQKeyboardManager.shared.enable = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        IQKeyboardManager.shared.enable = false
    }

    
    private func setUpSearchBar() {
        searchBar.delegate = self
    }
    
    func alterLayout() {
        recycleListTable.tableHeaderView = UIView()
        // search bar in section header
        recycleListTable.estimatedSectionHeaderHeight = 50
        // search bar in navigation bar
        //navigationItem.leftBarButtonItem = UIBarButtonItem(customView: searchBar)
        searchBar.placeholder = "請輸入回收物的名字"
    }
    
    // Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentAnimalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? recycleListCellTableViewCell else {
            return UITableViewCell()
        }
        cell.productName.text = currentAnimalArray[indexPath.row].productName
        cell.productCategory.text = currentAnimalArray[indexPath.row].productCategory.rawValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    // This two functions can be used if you want to show the search bar in the section header
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        return searchBar
    //    }
    
    //    // search bar in section header
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return UITableViewAutomaticDimension
    //    }
    
    // Search Bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        currentAnimalArray = animalArray.filter({ animal -> Bool in
            switch searchBar.selectedScopeButtonIndex {
            case 0:
                if searchText.isEmpty { return true }
                return animal.productName.lowercased().contains(searchText.lowercased())
            case 1:
                if searchText.isEmpty { return animal.productCategory == .unrecyclable }
                return animal.productName.lowercased().contains(searchText.lowercased()) &&
                    animal.productCategory == .unrecyclable
            case 2:
                if searchText.isEmpty { return animal.productCategory == .recyclable }
                return animal.productName.lowercased().contains(searchText.lowercased()) &&
                    animal.productCategory == .recyclable
            default:
                return false
            }
        })
        recycleListTable.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            currentAnimalArray = animalArray
        case 1:
            currentAnimalArray = animalArray.filter({ animal -> Bool in
                animal.productCategory == AnimalType.unrecyclable
            })
        case 2:
            currentAnimalArray = animalArray.filter({ animal -> Bool in
                animal.productCategory == AnimalType.recyclable
            })
        default:
            break
        }
        recycleListTable.reloadData()
    }
}

class Animal {
    let productName: String
    let productCategory: AnimalType
    
    init(productName: String, productCategory: AnimalType) {
        self.productName = productName
        self.productCategory = productCategory
    }
}

enum AnimalType: String {
    case recyclable = "可回收"
    case unrecyclable = "不可回收"
}
