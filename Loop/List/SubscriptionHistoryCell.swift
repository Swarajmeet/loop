//
//  SubscriptionHistoryCell.swift
//  Loop
//
//  Created by KHOSA on 22/07/19.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class SubscriptionHistoryCell: UITableViewCell {
    
    @IBOutlet var startDateLabel:UILabel!
    @IBOutlet var endDateLabel:UILabel!
    @IBOutlet var statusLabel:UILabel!
    @IBOutlet var amountLabel:UILabel!
    @IBOutlet var paymentModeLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

