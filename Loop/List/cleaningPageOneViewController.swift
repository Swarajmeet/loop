//
//  cleaningPageOneViewController.swift
//  Loop
//
//  Created by Jorch Wong on 8/4/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class cleaningPageOneViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        paperButton2.layer.cornerRadius = 10.0
        paperButton2.layer.masksToBounds = true
        plasticButton2.layer.cornerRadius = 10.0
        plasticButton2.layer.masksToBounds = true
        paperPackButton2.layer.cornerRadius = 10.0
        paperPackButton2.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var paperButton2: UIButton!
    @IBOutlet weak var plasticButton2: UIButton!
    @IBOutlet weak var paperPackButton2: UIButton!
    
    

}
