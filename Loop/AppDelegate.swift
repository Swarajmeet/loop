//
//  AppDelegate.swift
//  Loop
//
//  Created by Jorch Wong on 31/12/2018.
//  Copyright © 2018 Jorch Wong. All rights reserved.
//

import UIKit
import Stripe
import IQKeyboardManagerSwift
import NotificationCenter
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    
    private let publishableKey: String = "pk_live_OMmIw4uTkjF4nCPjmTsj2MEb00EK6n3qBz"
    private let baseURLString: String = "http://ec2-18-223-118-166.us-east-2.compute.amazonaws.com:3000/api/"
//    private let appleMerchantIdentifier: String = "merchant.com.example.Loop"
    private let appleMerchantIdentifier: String = "merchant.theloophk"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithDefaultBackground()
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }else{
            let appearance = UINavigationBar.appearance()
            appearance.barStyle = .default
            appearance.isTranslucent = true
        }
        
//        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
//        // Sets shadow (line below the bar) to a blank image
//        UINavigationBar.appearance().shadowImage = UIImage()
//        // Sets the translucent background color
//        UINavigationBar.appearance().backgroundColor = .clear
//        // Set translucent. (Default value is already true, so this can be removed if desired.)
//        UINavigationBar.appearance().isTranslucent = true
        
        //IQKeyboardManager.shared.isEnabled = true
        
        self.setupStripe()
        
        
        
//        IQKeyboardManager.shared.disabledToolbarClasses = [OrdersVc.self]
        
        
        
        self.registerForNotifications(application: application)
        if UserDefaultData.shared.isUserLoggedIn(){
                   goToHome()
               }else{
                   let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
                   let loginNavBar = storyboard.instantiateViewController(withIdentifier: "LoginNavBar") as! UINavigationController
                   window?.rootViewController = loginNavBar
                  UIView.transition(with:  window!,duration: 0.5,options: .transitionCrossDissolve,animations: nil,completion: nil)
               }
        return true
    }
    
    func goToHome(){
              
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let tabBarVc = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
              let appDelegate = UIApplication.shared.delegate as! AppDelegate
              appDelegate.window?.rootViewController = tabBarVc
              UIView.transition(with:  window!,duration: 0.5,options: .transitionCrossDissolve,animations: nil,completion: nil)
              
          }
       
       func getDeviceDetails(){
           DeviceDetails.shared.manufacture = "Apple"
           DeviceDetails.shared.os = UIDevice.current.systemName
           DeviceDetails.shared.model = deviceName()
           DeviceDetails.shared.osVersion = UIDevice.current.systemVersion
           DeviceDetails.shared.modelNumber = UIDevice.modelName
           
           if let conntryCode = Locale.current.regionCode{
               if let countryName = countryName(countryCode: conntryCode){
                   DeviceDetails.shared.countryName = countryName
               }
           }
           

           
       }
       
       func countryName(countryCode: String) -> String? {
           let current = Locale(identifier: "en_US")
           return current.localizedString(forRegionCode: countryCode)
       }
       
       func deviceName() -> String{
           switch  UIDevice.current.userInterfaceIdiom {
           case .unspecified:
               return "unspecified"
           case .phone:
               return "iPhone"
           case .tv:
               return "tv"
           case .pad:
               return "iPad"
           case .carPlay:
               return "carPlay"
           default:
               return "unspecified"
           }
           
       }
       
    
    
    
    func setupStripe() {
        
        // Stripe payment configuration
        STPPaymentConfiguration.shared().companyName = "Loop"
        
        if !publishableKey.isEmpty {
            STPPaymentConfiguration.shared().publishableKey = publishableKey
        }
        
        if !appleMerchantIdentifier.isEmpty {
            STPPaymentConfiguration.shared().appleMerchantIdentifier = appleMerchantIdentifier
        }
        
        
        // Stripe theme configuration
        STPTheme.default().primaryBackgroundColor = .loopVeryLightGrayColor
        STPTheme.default().primaryForegroundColor = .loopDarkBlueColor
        STPTheme.default().secondaryForegroundColor = .loopDarkGrayColor
        STPTheme.default().accentColor = .loopGreenColor
        
        // Main API client configuration
        MainAPIClient.shared.baseURLString = baseURLString
    }
    
    
    func registerForNotifications(application: UIApplication)  {
        
        
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        
        center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            // Enable or disable features based on authorization
            
            print(granted)
            print(error as Any)
            print(error?.localizedDescription as Any)

        }
        
        application.registerForRemoteNotifications()
        
        self.getNotificationSettings()
    }
    
    func getNotificationSettings()  {
        
        UNUserNotificationCenter.current().getNotificationSettings(){ (settings) in
            
            switch settings.authorizationStatus{
            case .notDetermined:
                print("notDetermined")
            case .authorized:
                print("authorized")
            case .denied:
                print("denied")
            case .provisional:
                print("provisional")
            }
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.hexString
        device_token = deviceTokenString
        print(deviceTokenString)
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    //MARK: - UNUserNotificationCenterDelegate
   
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound])
        
    }

}

